
QSensors
========

Desktop applet that monitor system temperature (CPU, HDD etc.)
It can show temperatures with color box or color bar, also
provides pretty temperature graph like [psensor](https://wpitchoune.net/psensor/).

Screenshots
-----------

* box ![alt text](img/box.png "Box")
* bar ![alt text](img/bar.png "Bar")
* graph ![alt text](img/graph.png "Graph")

Requirement
-----------

* linux
* C++17 compiler
* [Qt (>= 5.14.0)](https://www.qt.io/)
* [Boost (>= 1.72)](https://www.boost.org/)
* [udisks](https://freedesktop.org/wiki/Software/udisks/)
* [dbus](https://www.freedesktop.org/wiki/Software/dbus/)
* [lm-sensors](https://github.com/lm-sensors/lm-sensors)
* [QCustomPlot](https://www.qcustomplot.com/)
* [lxqt-panel](https://github.com/lxqt/lxqt-panel) (optional)

Building
--------

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
```

### cmake variable

* QCUSTOMPLOT_INCLUDE_DIR (default: /usr/include)
* QCUSTOMPLOT_LIBRARY_DIR (default: /usr/lib)
* BUILD_LXQT_PANEL_PLUGIN (default: On)
* LXQT_INCLUDE_DIR (default: /usr/include/lxqt)
