#include "box.hpp"

#include "widget.hpp"

#include "core/global.hpp"
#include "core/sensor.hpp"

namespace qsensors::widget {

static QColor
complementaryColor(QColor const& bg)
{
    return {
        255 - bg.red(),
        255 - bg.green(),
        255 - bg.blue()
    };
}

// Box

Box::
Box(Sensor const& sensor, Widget& parent)
    : QLabel { &parent }
    , m_widget { parent }
    , m_sensor { sensor }
{
    this->setAutoFillBackground(true);
    this->setAlignment(Qt::AlignCenter);

    this->connect(&m_sensor, &Sensor::updated,
                  this,      &Box::update);

    update();
}

Box::~Box() = default;

void Box::
update()
{
    updateColor();

    updateToolTip();
}

void Box::
updateColor()
{
    auto const& bgColor = m_widget.getBackgroundColor(m_sensor);

    if (bgColor) {
        auto const fgColor = complementaryColor(*bgColor);

        this->setStyleSheet(QSL("QLabel { color: %1; background-color: %2; }")
            .arg(fgColor.name()).arg(bgColor->name()) );
    }
    else {
        this->setStyleSheet(QSL(""));
    }
}

void Box::
updateToolTip()
{
    auto const value = m_sensor.value();

    if (value) {
        this->setText(QString::number(static_cast<int>(*value)));
    }
    else {
        this->setText(QSL(""));
    }

    this->setToolTip(widget::toolTipText(m_sensor));
}

} // namespace qsensors::widget
