#include "bar.hpp"

#include "widget.hpp"

#include "core/global.hpp"
#include "core/sensor.hpp"

namespace qsensors::widget {

Bar::
Bar(Sensor const& s, Widget& parent)
    : QProgressBar { &parent }
    , m_widget { parent }
    , m_sensor { s }
{
    this->setOrientation(Qt::Vertical);
    this->setTextVisible(false);
    this->setFixedWidth(8);
    this->setMinimumHeight(20);

    this->setMinimum(static_cast<int>(m_sensor.minimum().value_or(0)));
    this->setMaximum(static_cast<int>(m_sensor.maximum().value_or(0)));

    this->connect(&m_sensor, &Sensor::updated,
                  this,      &Bar::update);
}

Bar::~Bar() = default;

void Bar::
update(std::optional<double> const v)
{
    auto const& color = m_widget.getBackgroundColor(m_sensor);

    if (color) {
        QPalette palette = this->palette();
        palette.setColor(QPalette::Active, QPalette::Highlight, *color);
        palette.setColor(QPalette::Inactive, QPalette::Highlight, *color);
        this->setPalette(palette);
    }

    auto const value = static_cast<int>(v.value_or(0));
    this->setValue(value);

    this->setToolTip(widget::toolTipText(m_sensor));

    QProgressBar::update();
}

} // namespace qsensors::widget
