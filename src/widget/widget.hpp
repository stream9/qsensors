#ifndef QSENSORS_WIDGET_WIDGET_HPP
#define QSENSORS_WIDGET_WIDGET_HPP

#include "core/fwd/application.hpp"
#include "core/fwd/sensor.hpp"
#include "core/gradient.hpp"
#include "core/qmanaged_ptr.hpp"

#include <optional>
#include <vector>

#include <QString>
#include <QWidget>

class QColor;

namespace qsensors::widget {

class Widget : public QWidget
{
    Q_OBJECT
public:
    Widget(Application&, QWidget* parent = nullptr);
    ~Widget() override;

    std::optional<QColor> getBackgroundColor(Sensor const&) const;

private:
    void loadSettings();

    void addSensors(QStringList const& ids, QString const& type);
    void addSensor(Sensor const&, QString const& type);

    Q_SLOT void reloadSettings();

private:
    Application& m_app;
    Gradient m_gradient;
    std::vector<qmanaged_ptr<QWidget>> m_sensors;
};

QString toolTipText(Sensor const&);

} // namespace qsensors::widget

#endif // QSENSORS_WIDGET_WIDGET_HPP

