#include "widget.hpp"

#include "bar.hpp"
#include "box.hpp"

#include "core/application.hpp"
#include "core/sensor_range.hpp"
#include "core/settings.hpp"

#include <QHBoxLayout>

namespace qsensors::widget {

Widget::
Widget(Application& app, QWidget* parent/*= nullptr*/)
    : QWidget { parent }
    , m_app { app }
    , m_gradient { Qt::green, Qt::yellow, Qt::red, 0.6 }
{
    auto layout = make_qmanaged<QHBoxLayout>(this);
    layout->setContentsMargins(0, 3, 0, 3);
    layout->setSpacing(3);

    this->connect(&m_app.settings(), &Settings::changed,
                  this,              &Widget::reloadSettings);

    loadSettings();
}

Widget::~Widget() = default;

std::optional<QColor> Widget::
getBackgroundColor(Sensor const& sensor) const
{
    auto const value = sensor.value();
    auto const min = sensor.minimum().value_or(0);
    auto const max = sensor.maximum();

    if (!value || !max) {
        return std::nullopt;
    }

    auto const ratio = (*max - min) > 0 ? (*value - min) / (*max - min)
                                        : 0.0;

    return m_gradient.getColorAt(ratio);
}

void Widget::
loadSettings()
{
    auto& settings = m_app.settings();

    settings.beginGroup(QSL("widget"));

    auto const& ids = settings.value(QSL("sensors")).toStringList();
    auto const& lowColor = settings.value(QSL("lowColor")).toString();
    auto const& midColor = settings.value(QSL("midColor")).toString();
    auto const& highColor = settings.value(QSL("highColor")).toString();
    auto const& center = settings.value(QSL("center")).toDouble();
    auto const& type = settings.value(QSL("type")).toString();

    settings.endGroup();

    addSensors(ids, type);

    m_gradient.setColor1(lowColor);
    m_gradient.setColor2(midColor);
    m_gradient.setColor3(highColor);
    m_gradient.setCenter(center);
}

void Widget::
addSensors(QStringList const& ids, QString const& type)
{
    if (ids.isEmpty()) {
        for (auto const& sensor: m_app.sensors()) {
            addSensor(sensor, type);
        }
    }
    else {
        for (auto const& id: ids) {
            auto* const sensor = m_app.sensor(id);
            if (!sensor) continue;

            addSensor(*sensor, type);
        }
    }
}

void Widget::
addSensor(Sensor const& sensor, QString const& type)
{
    if (type == QSL("bar")) {
        auto box = make_qmanaged<Bar>(sensor, *this);
        this->layout()->addWidget(box.get());

        m_sensors.push_back(box);
    }
    else if (type == QSL("box")) {
        auto bar = make_qmanaged<Box>(sensor, *this);
        this->layout()->addWidget(bar.get());

        m_sensors.push_back(bar);
    }
}

void Widget::
reloadSettings()
{
    for (auto const& sensor: m_sensors) {
        sensor->deleteLater();
    }

    m_sensors.clear();

    loadSettings();
}

QString
toolTipText(Sensor const& sensor)
{
    auto const& value = sensor.value();
    auto const& min = sensor.minimum();
    auto const& max = sensor.maximum();

    QString text;
    QTextStream ts { &text };

    ts << sensor.label();

    if (value) {
       ts << QSL("\ncurrent: ") << *value;
    }
    else {
        ts << QSL("\nno value");
    }

    if (min) {
        ts << "\nmin: " << *min;
    }

    if (max) {
       ts << "\nmax: " << *max;

    }

    return text;
}

} // namespace qsensors::widget
