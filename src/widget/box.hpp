#ifndef QSENSORS_WIDGET_BOX_HPP
#define QSENSORS_WIDGET_BOX_HPP

#include "core/fwd/sensor.hpp"

#include <QLabel>

namespace qsensors::widget {

class Widget;

class Box : public QLabel
{
    Q_OBJECT
public:
    Box(Sensor const&, Widget& parent);
    ~Box() override;

private:
    Q_SLOT void update();

    void updateColor();
    void updateToolTip();

private:
    widget::Widget& m_widget;
    Sensor const& m_sensor;
};

} // namespace qsensors::widget

#endif // QSENSORS_WIDGET_BOX_HPP
