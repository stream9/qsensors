#ifndef QSENSORS_BAR_HPP
#define QSENSORS_BAR_HPP

#include "core/fwd/sensor.hpp"

#include <optional>

#include <QProgressBar>
#include <QString>

class QWidget;

namespace qsensors::widget {

class Widget;

class Bar : public QProgressBar
{
    Q_OBJECT
public:
    Bar(Sensor const&, Widget& parent);
    ~Bar() noexcept;

private:
    Q_SLOT void update(std::optional<double>);

private:
    Widget& m_widget;
    Sensor const& m_sensor;
};

} // namespace qsensors::widget

#endif // QSENSORS_BAR_HPP
