#ifndef QSENSORS_DEVICE_HPP
#define QSENSORS_DEVICE_HPP

#include "sensor.hpp"

#include <memory>
#include <vector>

#include <boost/range/adaptor/indirected.hpp>

namespace qsensors {

class Device
{
public:
    virtual ~Device() noexcept;

    // query
    auto sensors() const { return boost::adaptors::indirect(m_sensors); }

    // command
    void update();

protected:
    void addSensor(std::unique_ptr<Sensor>);

private:
    std::vector<std::unique_ptr<Sensor>> m_sensors; // non-null
};

} // namespace qsensors

#endif // QSENSORS_DEVICE_HPP
