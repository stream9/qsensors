#ifndef QSENSORS_CORE_FWD_SENSORS_HPP
#define QSENSORS_CORE_FWD_SENSORS_HPP

namespace qsensors {

struct SensorRecord;
class SensorRecords;
class History;

} // namespace qsensors

#endif // QSENSORS_CORE_FWD_SENSORS_HPP
