#ifndef QSENSORS_APPLICATION_HPP
#define QSENSORS_APPLICATION_HPP

#include "device.hpp"

#include <memory>
#include <vector>

#include <boost/range/adaptor/indirected.hpp>

#include <QTimer>

namespace qsensors {

class History;
class Settings;
class Sensor;
class SensorRange;

class Application : public QObject
{
    Q_OBJECT
public:
    Application();
    ~Application() noexcept;

    // acceessor
    Settings& settings() { return *m_settings; }
    auto devices() { return boost::adaptors::indirect(m_devices); }
    auto const& history() { return *m_history; }

    // query
    Sensor* sensor(QString const& id);
    SensorRange sensors();

    // command
    void showSettingsDialog(QWidget* parent = nullptr);

    // signal
    Q_SIGNAL void updated() const;
    Q_SIGNAL void settingChanged() const;

private:
    Q_SLOT void onTimer();
    Q_SLOT void loadSettings();

    void reloadSettings();

private:
    std::vector<std::unique_ptr<Device>> m_devices; // non-null
    std::unique_ptr<Settings> m_settings; // non-null
    std::unique_ptr<History> m_history; // non-null
    QTimer m_timer;
};

} // namespace qsensors

#endif // QSENSORS_APPLICATION_HPP
