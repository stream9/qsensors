#ifndef QSENSORS_CORE_SENSOR_RANGE_HPP
#define QSENSORS_CORE_SENSOR_RANGE_HPP

#include "core/application.hpp"
#include "core/device.hpp"

#include <boost/iterator/iterator_facade.hpp>
#include <boost/range/iterator_range.hpp>

namespace qsensors {

class Sensor;

class sensor_iterator : public boost::iterator_facade<
                                    sensor_iterator,
                                    Sensor&,
                                    std::input_iterator_tag,
                                    Sensor& >
{
public:
    sensor_iterator() = default;

    sensor_iterator(Application& app)
    {
        m_dev_it = app.devices().begin();
        m_dev_end = app.devices().end();

        if (m_dev_it != m_dev_end) {
            auto const& sensors = m_dev_it->sensors();
            m_sen_it = sensors.begin();
            m_sen_end = sensors.end();
        }
    }

private:
    friend class boost::iterator_core_access;

    void increment()
    {
        ++m_sen_it;

        if (m_sen_it == m_sen_end) {
            ++m_dev_it;

            if (m_dev_it != m_dev_end) {
                auto const& sensors = m_dev_it->sensors();
                m_sen_it = sensors.begin();
                m_sen_end = sensors.end();
            }
            else {
                assert(m_sen_it == m_sen_end);
                assert(m_dev_it == m_dev_end);
                m_sen_it = {};
                m_dev_it = {};
            }
        }
    }

    bool equal(sensor_iterator const& other) const
    {
        return m_dev_it == other.m_dev_it
            && m_sen_it == other.m_sen_it
            ;
    }

    Sensor& dereference() const
    {
        return *m_sen_it;
    }

private:
    using It1 = typename decltype(std::declval<Application>().devices())::iterator;
    using It2 = typename decltype(std::declval<Device>().sensors())::iterator;

    It1 m_dev_it;
    It1 m_dev_end;
    It2 m_sen_it;
    It2 m_sen_end;
};

class SensorRange : public boost::iterator_range<sensor_iterator>
{
public:
    SensorRange(Application& app)
        : boost::iterator_range<sensor_iterator> { sensor_iterator(app), sensor_iterator() }
    {}
};

} // namespace qsensors

#endif // QSENSORS_CORE_SENSOR_RANGE_HPP
