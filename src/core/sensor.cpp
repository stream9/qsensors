#include "sensor.hpp"

#include <cfloat>

#include "global.hpp"

namespace qsensors {

Sensor::
Sensor(QString const& id)
    : m_id { id }
    , m_range { DBL_MAX, DBL_MIN }
{}

Sensor::~Sensor() = default;

QString const& Sensor::
label() const
{
    if (m_label.isEmpty()) {
        return m_name;
    }
    else {
        return m_label;
    }
}

void Sensor::
setLabel(QString const& label)
{
    if (label == m_name) {
        m_label = QSL("");
    }
    else {
        m_label = label;
    }
}

void Sensor::
setValue(std::optional<double> const v)
{
    if (v) {
        if (m_range.upper < *v) {
            m_range.upper = *v;
        }
        if (m_range.lower > *v) {
            m_range.lower = *v;
        }
    }

    m_value = v;

    Q_EMIT updated(m_value);
}

} // namespace qsensors
