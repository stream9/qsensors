#ifndef QSENSORS_SENSOR_HPP
#define QSENSORS_SENSOR_HPP

#include <optional>

#include <QObject>
#include <QString>

namespace qsensors {

class Sensor : public QObject
{
    Q_OBJECT
public:
    struct Range
    {
        double lower;
        double upper;
    };

public:
    Sensor(QString const& id);
    ~Sensor() override;

    // accessor
    QString const& id() const { return m_id; }
    QString const& source() const { return m_source; }
    QString const& name() const { return m_name; }
    QString const& label() const;
    std::optional<double> value() const { return m_value; }
    std::optional<double> minimum() const { return m_minimum; }
    std::optional<double> maximum() const { return m_maximum; }
    Range range() const { return m_range; }

    // modifier
    void setLabel(QString const& l);

    // command
    virtual void update() = 0;

    // signal
    Q_SIGNAL void updated(std::optional<double>) const;

protected:
    void setSource(QString const& n) { m_source = n; }
    void setName(QString const& n) { m_name = n; }
    void setValue(std::optional<double>);
    void setMinimum(std::optional<double> const v) { m_minimum = v; }
    void setMaximum(std::optional<double> const v) { m_maximum = v; }

private:
    QString m_id;
    QString m_source;
    QString m_name;
    QString m_label;
    std::optional<double> m_value;
    std::optional<double> m_minimum;
    std::optional<double> m_maximum;
    Range m_range;
};

} // namespace qsensors

#endif // QSENSORS_SENSOR_HPP
