#ifndef QSENSORS_POINTER_HPP
#define QSENSORS_POINTER_HPP

#include "global.hpp"

#include "qmanaged_ptr.hpp"

template<typename T>
T&
to_ref(T* const ptr)
{
    assert(ptr);
    return *ptr;
}

#endif // QSENSORS_POINTER_HPP
