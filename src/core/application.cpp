#include "application.hpp"

#include "global.hpp"
#include "history.hpp"
#include "settings.hpp"

#include "lm_sensors/device.hpp"
#include "udisks/device.hpp"
#include "dialog/settings_dialog.hpp"

#include "sensor_range.hpp"

namespace qsensors {

Application::
Application()
    : m_settings { std::make_unique<Settings>() }
{
    m_devices.push_back(std::make_unique<udisks::Device>());
    m_devices.push_back(std::make_unique<lm_sensors::Device>());

    this->connect(m_settings.get(), &Settings::changed,
                  this,             &Application::reloadSettings);
    loadSettings();

    m_timer.setSingleShot(false);
    m_timer.setInterval(2000);
    m_timer.callOnTimeout(this, &Application::onTimer);
    m_timer.start();

    m_history = std::make_unique<History>(*this);
}

Application::~Application() = default;

Sensor* Application::
sensor(QString const& id)
{
    auto const& sensors = this->sensors();
    auto it = std::find_if(sensors.begin(), sensors.end(),
        [&](auto& sensor) {
            return sensor.id() == id;
        } );

    if (it == sensors.end()) {
        return nullptr;
    }
    else {
        return &*it;
    }
}

SensorRange Application::
sensors()
{
    return { *this };
}

void Application::
showSettingsDialog(QWidget* const parent)
{
    dialog::SettingsDialog dialog { *this, parent };

    dialog.exec();
}

void Application::
onTimer()
{
    for (auto const& device: m_devices) {
        device->update();
    }

    Q_EMIT updated();
}

void Application::
loadSettings()
{
    for (auto const& device: m_devices) {
        for (auto& sensor: device->sensors()) {
            auto const& id = sensor.id();

            auto const& label = m_settings->graphLabel(id);
            sensor.setLabel(label);
        }
    }
}

void Application::
reloadSettings()
{
    loadSettings();

    Q_EMIT settingChanged();
}

} // namespace qsensors
