#ifndef QSENSORS_CORE_GRADIENT_HPP
#define QSENSORS_CORE_GRADIENT_HPP

#include <QColor>

namespace qsensors {

class Gradient
{
public:
    Gradient(QColor const& col1, QColor const& col2, QColor const& col3, double center);
    ~Gradient() noexcept;

    // query
    QColor getColorAt(double ratio) const;

    // modifier
    void setColor1(QColor const&);
    void setColor2(QColor const&);
    void setColor3(QColor const&);
    void setCenter(double);

private:
    QColor m_col1, m_col2, m_col3;
    double m_center;
};

} // namespace qsensors

#endif // QSENSORS_CORE_GRADIENT_HPP
