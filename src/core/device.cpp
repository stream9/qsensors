#include "device.hpp"

#include "sensor.hpp"

namespace qsensors {

Device::~Device() = default;

void Device::
addSensor(std::unique_ptr<Sensor> s)
{
    m_sensors.push_back(std::move(s));
}

void Device::
update()
{
    for (auto const& sensor: m_sensors) {
        sensor->update();
    }
}

} // namespace qsensors
