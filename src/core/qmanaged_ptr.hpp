#ifndef QMANAGED_PTR_HPP
#define QMANAGED_PTR_HPP

#include "global.hpp"

#include <type_traits>

#include <QObject>

template<typename T>
class qmanaged_ptr
{
private:
    // comment out this because it will require T to be complete type
    //static_assert(std::is_base_of<QObject, T>::value, "T must be subtype of QObject");

    QObject* m_ptr; // not null

public:
    constexpr qmanaged_ptr() = delete;

    template <typename U,
              typename = std::enable_if_t<std::is_convertible_v<U*, T*>>>
    constexpr qmanaged_ptr(U* const ptr) noexcept
        : m_ptr { ptr }
    {
        assert(m_ptr);
    }

    template <typename U,
              typename = std::enable_if_t<std::is_convertible_v<U*, T*>>>
    constexpr qmanaged_ptr(qmanaged_ptr<U> const ptr) noexcept
        : m_ptr { ptr.get() }
    {
        assert(m_ptr);
    }

    constexpr qmanaged_ptr(qmanaged_ptr const&) = default;

    constexpr qmanaged_ptr& operator=(qmanaged_ptr const& ptr)
    {
        assert(m_ptr->parent());
        m_ptr = ptr;

        return *this;
    }

    constexpr qmanaged_ptr(qmanaged_ptr&&) noexcept = default;

    constexpr qmanaged_ptr& operator=(qmanaged_ptr&& ptr) noexcept
    {
        assert(m_ptr->parent());
        m_ptr = ptr;

        return *this;
    }

    template <typename U,
              typename = std::enable_if_t<std::is_convertible_v<U*, T*>>>
    constexpr qmanaged_ptr& operator=(U* const ptr) noexcept
    {
        assert(ptr);
        assert(m_ptr->parent());

        m_ptr = ptr;
        return *this;
    }

    template <typename U,
              typename = std::enable_if_t<std::is_convertible_v<U*, T*>>>
    constexpr qmanaged_ptr& operator=(qmanaged_ptr<U> const ptr) noexcept
    {
        assert(ptr);
        assert(m_ptr->parent());

        m_ptr = ptr;
        return *this;
    }

    ~qmanaged_ptr() noexcept
    {
        if (m_ptr) assert(m_ptr->parent());
    }

    constexpr T* get() const noexcept { return static_cast<T*>(m_ptr); }

    constexpr operator T*() const noexcept { return get(); }
    constexpr T* operator->() const noexcept { return get(); }
    constexpr auto& operator*() const noexcept { return *get(); }
};

template<typename T, typename... Args>
qmanaged_ptr<T>
make_qmanaged(Args&&... args)
{
    return qmanaged_ptr<T> { new T { std::forward<Args>(args)... } };
}

#endif // QMANAGED_PTR_HPP
