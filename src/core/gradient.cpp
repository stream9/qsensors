#include "gradient.hpp"

#include "global.hpp"

namespace qsensors {

static int
componentAt(int const v1, int const v2, double ratio)
{
    return v1 + static_cast<int>((v2 - v1) * ratio);
}

static QColor
colorAt(QColor const& col1, QColor const& col2, double const ratio)
{
    auto const r = componentAt(col1.red(), col2.red(), ratio);
    auto const g = componentAt(col1.green(), col2.green(), ratio);
    auto const b = componentAt(col1.blue(), col2.blue(), ratio);

    return { r, g, b };
}

// Gradient

Gradient::
Gradient(QColor const& col1, QColor const& col2, QColor const& col3, double const center)
    : m_col1 { col1 }
    , m_col2 { col2 }
    , m_col3 { col3 }
    , m_center { center }
{
    assert(0.0 <= center && center <= 1.0);
}

Gradient::~Gradient() = default;

QColor Gradient::
getColorAt(double const ratio) const
{
    if (ratio < 0.0) {
        return m_col1;
    }
    else if (ratio <= m_center) {
        auto const r = ratio / m_center;
        return colorAt(m_col1, m_col2, r);
    }
    else if (ratio <= 1.0) {
        auto const r = (ratio - m_center) / (1.0 - m_center);
        return colorAt(m_col2, m_col3, r);
    }
    else {
        return m_col3;
    }
}

void Gradient::
setColor1(QColor const& col)
{
    m_col1 = col;
}

void Gradient::
setColor2(QColor const& col)
{
    m_col2 = col;
}

void Gradient::
setColor3(QColor const& col)
{
    m_col3 = col;
}

void Gradient::
setCenter(double const v)
{
    m_center = v;
}

} // namespace qsensors
