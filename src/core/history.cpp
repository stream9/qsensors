#include "history.hpp"

#include "application.hpp"
#include "device.hpp"
#include "sensor.hpp"

#include <cassert>

namespace qsensors {

// SensorRecords

SensorRecords::
SensorRecords(Sensor& sensor)
{
    m_records.set_capacity(500);

    this->connect(&sensor, &Sensor::updated,
                  this,    &SensorRecords::append);
}

SensorRecords::~SensorRecords() = default;

void SensorRecords::
append(std::optional<double> const v)
{
    m_records.push_back({ std::time(nullptr), v.value_or(0) });
}

// History

History::
History(Application& app)
    : m_app { app }
{
    for (auto& device: m_app.devices()) {
        for (auto& sensor: device.sensors()) {
            m_records[&sensor] = std::make_unique<SensorRecords>(sensor);
        }
    }
}

History::~History() = default;

SensorRecords const& History::
records(Sensor const& sensor) const
{
    auto const it = m_records.find(&sensor);
    assert(it != m_records.end());

    return *it->second;
}

} // namespace qsensors
