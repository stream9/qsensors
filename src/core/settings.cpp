#include "settings.hpp"

#include "global.hpp"

#include <QDir>
#include <QStandardPaths>
#include <QString>

namespace qsensors {

static QDir
directory()
{
    auto const& dirName = QSL("qsensors");
    QDir dir {
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
    };

    if (!dir.exists(dirName)) {
        dir.mkdir(dirName);
    }
    dir.cd(dirName);

    return dir;
}

static QString
path()
{
    static QString path = directory().filePath(QSL("settings.ini"));

    return path;
}

// Settings

Settings::
Settings()
    : QSettings { path(), QSettings::IniFormat }
{}

Settings::~Settings() = default;

QString Settings::
graphLabel(QString const& id)
{
    this->beginGroup(QSL("label"));

    auto const& label = this->value(id).toString();

    this->endGroup();

    return label;
}

void Settings::
setGraphLabel(QString const& id, QString const& name, QString const& l)
{
    this->beginGroup(QSL("label"));

    auto const& label = l == name ? QSL("") : l;

    if (label.isEmpty()) {
        this->remove(id);
    }
    else {
        this->setValue(id, label);
    }

    this->endGroup();
}

} // namespace qsensors
