#ifndef QSENSORS_CORE_SETTINGS_HPP
#define QSENSORS_CORE_SETTINGS_HPP

#include <QSettings>

class QString;

namespace qsensors {

class Settings : public QSettings
{
    Q_OBJECT
public:
    Settings();
    ~Settings() override;

    // query
    QString graphLabel(QString const& id);

    // modifier
    void setGraphLabel(QString const& id, QString const& name, QString const& label);

    // signal
    Q_SIGNAL void changed() const;
};

} // namespace qsensors

#endif // QSENSORS_CORE_SETTINGS_HPP
