#ifndef QSENSORS_CORE_HISTORY_HPP
#define QSENSORS_CORE_HISTORY_HPP

#include "fwd/application.hpp"
#include "fwd/sensor.hpp"

#include <ctime>
#include <optional>

#include <boost/circular_buffer.hpp>
#include <boost/container/flat_map.hpp>

#include <QObject>

namespace qsensors {

struct SensorRecord
{
    std::time_t time;
    double value;
};

class SensorRecords : public QObject
{
    Q_OBJECT
public:
    SensorRecords(Sensor&);
    ~SensorRecords() noexcept;

    // iterator
    auto begin() const { return m_records.begin(); }
    auto end() const { return m_records.end(); }

    // query
    auto size() const { return m_records.size(); }

private:
    Q_SLOT void append(std::optional<double> value);

private:
    boost::circular_buffer<SensorRecord> m_records;
};

class History
{
public:
    History(Application&);
    ~History() noexcept;

    // query
    SensorRecords const& records(Sensor const&) const;

private:
    Application& m_app;
    boost::container::flat_map<
        Sensor const*, std::unique_ptr<SensorRecords> > m_records;
};

} // namespace qsensors

#endif // QSENSORS_CORE_HISTORY_HPP
