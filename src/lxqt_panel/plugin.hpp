#ifndef QSENSORS_LXQT_PANEL_PLUGIN_HPP
#define QSENSORS_LXQT_PANEL_PLUGIN_HPP

#include "core/fwd/application.hpp"
#include "widget/fwd/widget.hpp"
#include "plot/fwd/widget.hpp"

#include <memory>

#include <QPointer>

#include <ilxqtpanelplugin.h>

class QEvent;

namespace qsensors::lxqt_panel {

class Plugin : public QObject, public ILXQtPanelPlugin
{
public:
    Plugin(ILXQtPanelPluginStartupInfo const&);
    ~Plugin() override;

    // override ILXQtPanelPlugin
    ILXQtPanelPlugin::Flags flags() const override;
    QWidget* widget() override;
    QString themeId() const override;
    QDialog* configureDialog() override;
    void realign() override;

    // override QObject
    bool eventFilter(QObject* watched, QEvent*) override;

private:
    void showPlotWindow();

private:
    std::unique_ptr<Application> m_app; // non-null
    std::unique_ptr<widget::Widget> m_widget; // non-null
    QPointer<plot::Widget> m_plot;
};

class Library : public QObject, public ILXQtPanelPluginLibrary
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "lxqt.org/Panel/PluginInterface/3.0")
    Q_INTERFACES(ILXQtPanelPluginLibrary)
public:
    // override ILXQtPanelPluginLibrary
    ILXQtPanelPlugin* instance(ILXQtPanelPluginStartupInfo const&) const override;
};

} // namespace qsensors::lxqt_panel

#endif // QSENSORS_LXQT_PANEL_PLUGIN_HPP
