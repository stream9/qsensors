#include "plugin.hpp"

#include "core/application.hpp"
#include "core/global.hpp"
#include "dialog/settings_dialog.hpp"
#include "plot/widget.hpp"
#include "widget/widget.hpp"

#include <QMouseEvent>

namespace qsensors::lxqt_panel {

static bool
isLeftButtonPressed(QEvent* const ev)
{
    auto* const event = dynamic_cast<QMouseEvent* const>(ev);
    if (!event) return false;

    return event->button() & Qt::LeftButton;
}

// Plugin

Plugin::
Plugin(ILXQtPanelPluginStartupInfo const& startupInfo)
    : ILXQtPanelPlugin { startupInfo }
    , m_app { std::make_unique<Application>() }
    , m_widget { std::make_unique<widget::Widget>(*m_app) }
{
    m_widget->installEventFilter(this);
}

Plugin::~Plugin() = default;

QWidget* Plugin::
widget()
{
    return m_widget.get();
}

ILXQtPanelPlugin::Flags Plugin::
flags() const
{
    return ILXQtPanelPlugin::PreferRightAlignment
         | ILXQtPanelPlugin::HaveConfigDialog;
}

QString Plugin::
themeId() const
{
    return QSL("Sensors");
}

QDialog* Plugin::
configureDialog()
{
    auto* const dialog = new dialog::SettingsDialog { *m_app };
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    return dialog;
}

void Plugin::
realign()
{
    //TODO
}

bool Plugin::
eventFilter(QObject* const watched, QEvent* const ev)
{
    if (watched == m_widget.get() && isLeftButtonPressed(ev)) {
        showPlotWindow();
        return true;
    }

    return QObject::eventFilter(watched, ev);
}

void Plugin::
showPlotWindow()
{
    if (m_plot.isNull()) {
        m_plot = new plot::Widget { *m_app };
        m_plot->setAttribute(Qt::WA_DeleteOnClose);
    }

    m_plot->show();
}

// Library

ILXQtPanelPlugin* Library::
instance(ILXQtPanelPluginStartupInfo const& startupInfo) const
{
    return new Plugin(startupInfo);
}

} // namespace qsensors::lxqt_panel
