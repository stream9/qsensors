#include "device.hpp"

#include "sensor.hpp"

#include "core/global.hpp"

#include <boost/iterator/iterator_facade.hpp>

#include <sensors/sensors.h>

namespace qsensors::lm_sensors {

struct begin_tag {};

// const_chip_iterator

class const_chip_iterator : public boost::iterator_facade<
                                const_chip_iterator,
                                ::sensors_chip_name const,
                                boost::forward_traversal_tag >
{
public:
    const_chip_iterator(begin_tag)
    {
        increment();
    }

    const_chip_iterator() = default;

private:
    friend class boost::iterator_core_access;

    void increment()
    {
        m_chip = ::sensors_get_detected_chips(nullptr, &m_n);
    }

    bool equal(const_chip_iterator const& other) const
    {
        return m_chip == other.m_chip;
    }

    ::sensors_chip_name const& dereference() const
    {
        return *m_chip;
    }

private:
    int m_n = 0;
    ::sensors_chip_name const* m_chip = nullptr;
};

const_chip_iterator
begin(const_chip_iterator const it)
{
    return it;
}

const_chip_iterator
end(const_chip_iterator)
{
    return const_chip_iterator {};
}

const_chip_iterator
make_chip_iterator()
{
    return const_chip_iterator { begin_tag() };
}

// const_feature_iterator

class const_feature_iterator : public boost::iterator_facade<
                                const_feature_iterator,
                                ::sensors_feature const,
                                boost::forward_traversal_tag >
{
public:
    const_feature_iterator(::sensors_chip_name const& chip)
        : m_chip { &chip }
    {
        increment();
    }

    const_feature_iterator() = default;

private:
    friend class boost::iterator_core_access;

    void increment()
    {
        m_feature = ::sensors_get_features(m_chip, &m_n);
    }

    bool equal(const_feature_iterator const& other) const
    {
        return m_feature == other.m_feature;
    }

    ::sensors_feature const& dereference() const
    {
        return *m_feature;
    }

private:
    ::sensors_chip_name const* m_chip = nullptr;
    ::sensors_feature const* m_feature = nullptr;
    int m_n = 0;
};

const_feature_iterator
begin(const_feature_iterator const it)
{
    return it;
}

const_feature_iterator
end(const_feature_iterator)
{
    return const_feature_iterator {};
}

const_feature_iterator
make_feature_iterator(::sensors_chip_name const& chip)
{
    return const_feature_iterator { chip };
}

// const_subfeature_iterator
class const_subfeature_iterator : public boost::iterator_facade<
                                    const_subfeature_iterator,
                                    ::sensors_subfeature const,
                                    boost::forward_traversal_tag >
{
public:
    const_subfeature_iterator(::sensors_chip_name const& chip,
                              ::sensors_feature const& feature)
        : m_chip { &chip }
        , m_feature { &feature }
    {
        increment();
    }

    const_subfeature_iterator() = default;

private:
    friend class boost::iterator_core_access;

    void increment()
    {
        m_subfeature = ::sensors_get_all_subfeatures(m_chip, m_feature, &m_n);
    }

    bool equal(const_subfeature_iterator const& other) const
    {
        return m_subfeature == other.m_subfeature;
    }

    ::sensors_subfeature const& dereference() const
    {
        return *m_subfeature;
    }

private:
    ::sensors_chip_name const* m_chip = nullptr;
    ::sensors_feature const* m_feature = nullptr;
    ::sensors_subfeature const* m_subfeature = nullptr;
    int m_n = 0;
};

const_subfeature_iterator
begin(const_subfeature_iterator const it)
{
    return it;
}

const_subfeature_iterator
end(const_subfeature_iterator)
{
    return const_subfeature_iterator {};
}

const_subfeature_iterator
make_subfeature_iterator(::sensors_chip_name const& chip,
                         ::sensors_feature const& feature)
{
    return const_subfeature_iterator { chip, feature };
}

static uint64_t s_ref = 0;

static void
init_libsensors()
{
    if (s_ref == 0) {
        if (::sensors_init(nullptr) != 0) {
            throw std::runtime_error("couldn't initialize libsensors");
        }
    }
    else {
        ++s_ref;
    }
}

static void
deinit_libsensors()
{
    --s_ref;
    if (s_ref == 0) {
        ::sensors_cleanup();
    }
}

// Device

Device::
Device()
{
    init_libsensors();

    for (auto const& chip: make_chip_iterator()) {
        for (auto const& feature: make_feature_iterator(chip)) {
            if (feature.type != SENSORS_FEATURE_TEMP) continue;

            auto* const input = ::sensors_get_subfeature(
                              &chip, &feature, SENSORS_SUBFEATURE_TEMP_INPUT);
            if (input == nullptr) continue;

            auto* const name = ::sensors_get_label(&chip, &feature);

            this->addSensor(std::make_unique<Sensor>(name, chip, feature, *input));
        }
    }
}

Device::
~Device()
{
    deinit_libsensors();
}

} // namespace qsensors::lm_sensors
