#ifndef QSENSORS_LM_SENSORS_SENSOR_HPP
#define QSENSORS_LM_SENSORS_SENSOR_HPP

#include "core/sensor.hpp"

#include <optional>

#include <sensors/sensors.h>

class QString;

namespace qsensors::lm_sensors {

class Sensor : public qsensors::Sensor
{
public:
    Sensor(QString const& name,
           ::sensors_chip_name const&,
           ::sensors_feature const&,
           ::sensors_subfeature const& input );
    ~Sensor() override;

    // query
    std::optional<QString> type() const;
    std::optional<double> critical() const;
    std::optional<double> min_critical() const;
    std::optional<double> emergency() const;
    std::optional<double> lowest() const;
    std::optional<double> highest() const;

    // command
    void update() override;

private:
    std::optional<double>
        subfeatureValue(::sensors_subfeature_type) const;

private:
    ::sensors_chip_name const& m_chip;
    ::sensors_feature const& m_feature;
    ::sensors_subfeature const& m_input;
};

} // namespace qsensors::lm_sensors

#endif // QSENSORS_LM_SENSORS_SENSOR_HPP
