#include "sensor.hpp"

#include "core/global.hpp"

#include <QCryptographicHash>

namespace qsensors::lm_sensors {

static QString
toQString(::sensors_chip_name const& chip)
{
    char buf[256];
    auto n = ::sensors_snprintf_chip_name(buf, 256, &chip);
    assert(n >= 0);

    return QString::fromUtf8(buf, static_cast<qsizetype>(n));
}

static QString
generateId(::sensors_chip_name const& chip,
           QString const& name)
{
    return QCryptographicHash::hash(
        QSL("lm_sensors_%1_%2").arg(toQString(chip)).arg(name).toUtf8(),
        QCryptographicHash::Md5
    ).toHex();
}

// Sensor

Sensor::
Sensor(QString const& name,
       ::sensors_chip_name const& chip,
       ::sensors_feature const& feature,
       ::sensors_subfeature const& input )
    : qsensors::Sensor { generateId(chip, name) }
    , m_chip { chip }
    , m_feature { feature }
    , m_input { input }
{
    this->setSource(toQString(chip));
    this->setName(name);

    auto const min = subfeatureValue(SENSORS_SUBFEATURE_TEMP_MIN);
    if (min) {
        this->setMinimum(*min);
    }

    auto const max = subfeatureValue(SENSORS_SUBFEATURE_TEMP_MAX);
    if (max) {
        this->setMaximum(*max);
    }

    update();
}

Sensor::~Sensor() = default;

std::optional<QString> Sensor::
type() const
{
    auto const value = subfeatureValue(SENSORS_SUBFEATURE_TEMP_TYPE);
    if (!value) return {};

    assert(1 <= *value && *value <= 6);
    switch (static_cast<int>(*value)) {
        case 1:
            return QSL("CPU embedded diode");
        case 2:
            return QSL("3904 transistor");
        case 3:
            return QSL("thermal diode");
        case 4:
            return QSL("thermistor");
        case 5:
            return QSL("AMD AMDSI");
        case 6:
            return QSL("Intel PECI");
        default:
            return {};
    }
}

std::optional<double> Sensor::
critical() const
{
    return subfeatureValue(SENSORS_SUBFEATURE_TEMP_CRIT);
}

std::optional<double> Sensor::
min_critical() const
{
    return subfeatureValue(SENSORS_SUBFEATURE_TEMP_LCRIT);
}

std::optional<double> Sensor::
emergency() const
{
    return subfeatureValue(SENSORS_SUBFEATURE_TEMP_EMERGENCY);
}

std::optional<double> Sensor::
lowest() const
{
    return subfeatureValue(SENSORS_SUBFEATURE_TEMP_LOWEST);
}

std::optional<double> Sensor::
highest() const
{
    return subfeatureValue(SENSORS_SUBFEATURE_TEMP_HIGHEST);
}

void Sensor::
update()
{
    constexpr int success = 0;
    double value = 0;

    if (::sensors_get_value(&m_chip, m_input.number, &value) != success) {
        //TODO
    }

    this->setValue(value);
}

std::optional<double> Sensor::
subfeatureValue(::sensors_subfeature_type const type) const
{
    auto* const subfeature = ::sensors_get_subfeature(
                      &m_chip, &m_feature, type);
    if (!subfeature) return {};

    double value = 0;
    constexpr int success = 0;
    if (::sensors_get_value(&m_chip, subfeature->number, &value) != success) {
        return {};
    }

    return value;
}

} // namespace qsensors::lm_sensors
