#ifndef QSENSORS_LM_SENSORS_DEVICE_HPP
#define QSENSORS_LM_SENSORS_DEVICE_HPP

#include "core/device.hpp"

namespace qsensors::lm_sensors {

class Sensor;

class Device : public qsensors::Device
{
public:
    Device();
    ~Device() noexcept override;
};

} // namespace qsensors::lm_sensors

#endif // QSENSORS_LM_SENSORS_DEVICE_HPP
