#include "gradient_label.hpp"

#include "core/global.hpp"
#include "core/pointer.hpp"

#include <QBrush>
#include <QLinearGradient>
#include <QPalette>
#include <QResizeEvent>

namespace qsensors::dialog {

GradientLabel::
GradientLabel(QWidget& parent)
    : QLabel { &parent }
{
    this->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    this->setMinimumHeight(20);
    this->setFocusPolicy(Qt::NoFocus);
    this->setAutoFillBackground(true);
}

GradientLabel::~GradientLabel() = default;

void GradientLabel::
setColor1(QColor const& col)
{
    m_col1 = col;
}

void GradientLabel::
setColor2(QColor const& col)
{
    m_col2 = col;
}

void GradientLabel::
setColor3(QColor const& col)
{
    m_col3 = col;
}

void GradientLabel::
setCenter(double const v)
{
    assert(0 <= v && v <= 1.0);

    m_center = v;

    updateGradient();
}

void GradientLabel::
mousePressEvent(QMouseEvent*)
{
    Q_EMIT pressed();
}

void GradientLabel::
resizeEvent(QResizeEvent*)
{
    updateGradient();
}

void GradientLabel::
updateGradient()
{
    if (!m_col1.isValid() || !m_col2.isValid() || !m_col3.isValid()) return;

    auto const width = static_cast<double>(this->width());

    QLinearGradient g { 0, 0, width, 0 };
    g.setColorAt(0.0, m_col1);
    g.setColorAt(m_center, m_col2);
    g.setColorAt(1.0, m_col3);

    auto pal = this->palette();
    pal.setBrush(QPalette::Window, g);
    this->setPalette(pal);
}

} // namespace qsensors::dialog
