#include "sensor_list.hpp"

#include "sensor_chooser.hpp"
#include "sensor_list_item.hpp"
#include "settings_dialog.hpp"

#include "core/application.hpp"
#include "core/pointer.hpp"

#include <QAction>
#include <QBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QToolButton>

namespace qsensors::dialog {

SensorList::
SensorList(SettingsDialog& dialog)
    : m_dialog { dialog }
    , m_list { make_qmanaged<QListWidget>(this) }
    , m_chooseButton { make_qmanaged<QToolButton>(this) }
    , m_moveUpAction { make_qmanaged<QAction>(this) }
    , m_moveDownAction { make_qmanaged<QAction>(this) }
{
    buildActions();
    buildUI();

    this->connect(m_list.get(), &QListWidget::currentItemChanged,
                  this,         &SensorList::updateActions);
    this->connect(m_list.get(), &QListWidget::currentItemChanged,
                  this,         &SensorList::onCurrentItemChanged);

    this->connect(m_chooseButton.get(), &QToolButton::clicked,
                  this,                 &SensorList::openChooser);

    updateActions();
}

SensorList::~SensorList() = default;

QStringList SensorList::
ids() const
{
    QStringList result;

    for (int i = 0; i < m_list->count(); ++i) {
        auto& item = to_ref(dynamic_cast<SensorListItem*>(m_list->item(i)));
        result.push_back(item.sensor().id());
    }

    return result;
}

Sensor* SensorList::
item(int const index) const
{
    auto* const item = dynamic_cast<SensorListItem*>(m_list->item(index));

    return item ? &item->sensor() : nullptr;
}

Sensor* SensorList::
currentItem() const
{
    auto* const item = dynamic_cast<SensorListItem*>(m_list->currentItem());

    return item ? &item->sensor() : nullptr;
}

int SensorList::
currentIndex() const
{
    return m_list->currentRow();
}

void SensorList::
setItems(QStringList const& ids)
{
    m_list->clear();

    auto& app = m_dialog.application();

    for (auto const& id: ids) {
        auto* const sensor = app.sensor(id);
        if (sensor == nullptr) continue;

        auto* const item = new SensorListItem { *sensor };
        m_list->addItem(item); // QListWidget take ownership
    }

    updateActions();
}

void SensorList::
buildActions()
{
    auto& style = to_ref(this->style());

    m_moveUpAction->setIcon(style.standardIcon(QStyle::SP_ArrowUp));
    m_moveUpAction->setEnabled(false);
    this->connect(m_moveUpAction.get(), &QAction::triggered,
                  this,                 &SensorList::moveUp);

    m_moveDownAction->setIcon(style.standardIcon(QStyle::SP_ArrowDown));
    m_moveDownAction->setEnabled(false);
    this->connect(m_moveDownAction.get(), &QAction::triggered,
                  this,                   &SensorList::moveDown);
}

void SensorList::
buildUI()
{
    auto outer = make_qmanaged<QVBoxLayout>(this);
    outer->setContentsMargins(0, 0, 0, 0);

    auto label = make_qmanaged<QLabel>(QSL("Label:"), this);
    outer->addWidget(label.get());
    outer->addWidget(m_list.get());

    auto inner = make_qmanaged<QHBoxLayout>();

    m_chooseButton->setText(QSL("Add / Remove"));
    m_chooseButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    inner->addWidget(m_chooseButton.get(), 1);

    auto upButton = make_qmanaged<QToolButton>(this);
    upButton->setDefaultAction(m_moveUpAction.get());
    inner->addWidget(upButton.get());

    auto downButton = make_qmanaged<QToolButton>(this);
    downButton->setDefaultAction(m_moveDownAction.get());
    inner->addWidget(downButton.get());

    inner->addStretch();

    outer->addLayout(inner.get());
}

void SensorList::
onCurrentItemChanged(QListWidgetItem* const item_, QListWidgetItem*)
{
    auto* const item = dynamic_cast<SensorListItem*>(item_);

    if (item == nullptr) {
        Q_EMIT currentItemChanged(-1);
    }
    else {
        Q_EMIT currentItemChanged(m_list->row(item));
    }
}

void SensorList::
updateActions()
{
    auto const row = m_list->currentRow();

    m_moveUpAction->setEnabled(row > 0);
    m_moveDownAction->setEnabled(row != -1 && row + 1 < m_list->count());
}

void SensorList::
moveUp()
{
    auto& item = to_ref(m_list->currentItem());

    auto const row = m_list->row(&item);
    assert(row > 0);

    m_list->takeItem(row);
    m_list->insertItem(row - 1, &item);
    m_list->setCurrentItem(&item);

    updateActions();

    m_dialog.setEdited();
}

void SensorList::
moveDown()
{
    auto& item = to_ref(m_list->currentItem());

    auto const row = m_list->row(&item);
    assert(row >= 0);

    m_list->takeItem(row);
    m_list->insertItem(row + 1, &item);
    m_list->setCurrentItem(&item);

    updateActions();

    m_dialog.setEdited();
}

void SensorList::
openChooser()
{
    SensorChooser chooser { m_dialog };

    chooser.chooseSensors(ids());
    if (chooser.exec() == QDialog::Accepted) {
        setItems(chooser.choosedSensors());
        m_dialog.setEdited();
    }
}

} // namespace qsensors::dialog
