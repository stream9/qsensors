#include "settings_dialog.hpp"

#include "sensors_page.hpp"
#include "widget_page.hpp"
#include "plot_page.hpp"

#include "core/application.hpp"
#include "core/pointer.hpp"
#include "core/settings.hpp"

#include <QDialogButtonBox>
#include <QPushButton>
#include <QTabWidget>
#include <QVBoxLayout>

namespace qsensors::dialog {

SettingsDialog::
SettingsDialog(Application& app, QWidget* const parent/*= nullptr*/)
    : QDialog { parent }
    , m_app { app }
    , m_sensorsPage { make_qmanaged<SensorsPage>(*this) }
    , m_widgetPage { make_qmanaged<WidgetPage>(*this) }
    , m_plotPage { make_qmanaged<PlotPage>(*this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto tab = make_qmanaged<QTabWidget>(this);
    layout->addWidget(tab.get());

    tab->addTab(m_sensorsPage.get(), QSL("Sensors"));
    tab->addTab(m_widgetPage.get(), QSL("Widget"));
    tab->addTab(m_plotPage.get(), QSL("Plot"));

    auto& ok = to_ref(m_buttons->addButton(QDialogButtonBox::Ok));
    ok.setEnabled(false);
    m_buttons->addButton(QDialogButtonBox::Cancel);

    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);

    layout->addWidget(m_buttons.get());

    this->connect(this, &QDialog::accepted,
                  this, &SettingsDialog::save);

    this->resize(640, 400);
}

SettingsDialog::~SettingsDialog() = default;

void SettingsDialog::
setEdited(bool const v/*= true*/)
{
    m_edited = v;

    auto& ok = to_ref(m_buttons->button(QDialogButtonBox::Ok));
    ok.setEnabled(v);
}

void SettingsDialog::
save() const
{
    m_sensorsPage->save();
    m_widgetPage->save();
    m_plotPage->save();

    Q_EMIT m_app.settings().changed();
}

} // namespace qsensors::dialog
