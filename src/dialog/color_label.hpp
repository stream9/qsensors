#ifndef QSENSORS_DIALOG_COLOR_LABEL_HPP
#define QSENSORS_DIALOG_COLOR_LABEL_HPP

#include <QLabel>

class QColor;
class QMouseEvent;
class QWidget;

namespace qsensors::dialog {

class ColorLabel : public QLabel
{
    Q_OBJECT
public:
    ColorLabel(QWidget& parent);
    ~ColorLabel() override;

    // query
    QColor color() const;

    // modifier
    void setColor(QColor const&);

    // signal
    Q_SIGNAL void pressed() const;

protected:
    // override QWidget
    void mousePressEvent(QMouseEvent*) override;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_COLOR_LABEL_HPP
