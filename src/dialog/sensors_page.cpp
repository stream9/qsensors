#include "sensors_page.hpp"

#include "settings_dialog.hpp"

#include <QCheckBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QListWidget>
#include <QSplitter>
#include <QStackedLayout>

#include "core/application.hpp"
#include "core/pointer.hpp"
#include "core/settings.hpp"
#include "lm_sensors/sensor.hpp"
#include "udisks/sensor.hpp"

namespace qsensors::dialog {

SensorsPage::
SensorsPage(SettingsDialog& dialog)
    : QWidget { &dialog }
    , m_dialog { dialog }
    , m_list { make_qmanaged<QListWidget>(this) }
    , m_detail { make_qmanaged<QWidget>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto splitter = make_qmanaged<QSplitter>(Qt::Horizontal, this);
    layout->addWidget(splitter.get());

    this->connect(m_list.get(), &QListWidget::currentItemChanged,
                  this,         &SensorsPage::onCurrentChanged );
    splitter->addWidget(m_list.get());

    make_qmanaged<QStackedLayout>(m_detail.get());
    splitter->addWidget(m_detail.get());

    QList<int> sizes;
    sizes.push_back(100);
    sizes.push_back(400);
    splitter->setSizes(sizes);

    m_list->resize(200, m_list->height());

    loadSensors();
}

SensorsPage::~SensorsPage() = default;

void SensorsPage::
save() const
{
    auto& settings = m_dialog.application().settings();
    auto& layout = detailLayout();

    for (auto i = 0; i < layout.count(); ++i) {
        auto& widget = to_ref(dynamic_cast<SensorInfoWidget*>(layout.widget(i)));
        auto const& id = widget.sensor().id();
        auto const& name = widget.sensor().name();

        settings.setGraphLabel(id, name, widget.label());
    }
}

void SensorsPage::
loadSensors()
{
    auto& app = m_dialog.application();
    auto& detail = detailLayout();

    for (auto& device: app.devices()) {
        for (auto& sensor: device.sensors()) {
            m_list->addItem(new SensorItem(sensor, *m_list));

            auto& widget = makeInfoWidget(sensor);
            detail.addWidget(&widget);
        }
    }
}

SensorInfoWidget& SensorsPage::
makeInfoWidget(Sensor& sensor)
{
    if (auto* const s = dynamic_cast<udisks::Sensor*>(&sensor); s) {
        return *make_qmanaged<AtaWidget>(*s, *this);
    }
    else if (auto* const s = dynamic_cast<lm_sensors::Sensor*>(&sensor); s) {
        return *make_qmanaged<LmSensorsWidget>(*s, *this);
    }
    else {
        return *make_qmanaged<SensorInfoWidget>(sensor, *this);
    }
}

QStackedLayout& SensorsPage::
detailLayout() const
{
    return to_ref(dynamic_cast<QStackedLayout*>(m_detail->layout()));
}

void SensorsPage::
onCurrentChanged(QListWidgetItem* const current, QListWidgetItem*)
{
    if (current == nullptr) return;

    auto const row = m_list->row(current);
    assert(row >= 0);

    auto& layout = to_ref(dynamic_cast<QStackedLayout*>(m_detail->layout()));
    layout.setCurrentIndex(row);
}

// SensorItem

SensorItem::
SensorItem(Sensor& s, QListWidget& parent)
    : QListWidgetItem { s.name(), &parent }
    , m_sensor { s }
{}

SensorItem::~SensorItem() = default;

// SensorInfoWidget

SensorInfoWidget::
SensorInfoWidget(Sensor& sensor, SensorsPage& page)
    : QWidget { &page }
    , m_page { page }
    , m_sensor { sensor }
    , m_label { make_qmanaged<QLineEdit>(this) }
    , m_id { make_qmanaged<QLabel>(this) }
    , m_source { make_qmanaged<QLabel>(this) }
    , m_name { make_qmanaged<QLabel>(this) }
    , m_minimum { make_qmanaged<QLabel>(this) }
    , m_maximum { make_qmanaged<QLabel>(this) }
{
    auto layout = make_qmanaged<QFormLayout>(this);

    m_label->setText(sensor.label());
    this->connect(m_label.get(), &QLineEdit::textEdited,
                  this,          &SensorInfoWidget::onEdited );
    layout->addRow(QSL("Label:"), m_label.get());

    m_id->setText(m_sensor.id());
    layout->addRow(QSL("ID:"), m_id.get());

    m_source->setText(m_sensor.source());
    layout->addRow(QSL("Source:"), m_source.get());

    m_name->setText(m_sensor.name());
    layout->addRow(QSL("Name:"), m_name.get());

    m_minimum->setText(QString::number(m_sensor.minimum().value_or(0)));
    layout->addRow(QSL("Minimum:"), m_minimum.get());

    m_maximum->setText(QString::number(m_sensor.maximum().value_or(0)));
    layout->addRow(QSL("Maximum:"), m_maximum.get());
}

SensorInfoWidget::~SensorInfoWidget() = default;

QString SensorInfoWidget::
label() const
{
    return m_label->text();
}

void SensorInfoWidget::
onEdited() const
{
    m_page.dialog().setEdited(true);
}

// AtaWidget

AtaWidget::
AtaWidget(udisks::Sensor& sensor, SensorsPage& page)
    : SensorInfoWidget { sensor, page }
{}

AtaWidget::~AtaWidget() = default;

// LmSensorWidget

LmSensorsWidget::
LmSensorsWidget(lm_sensors::Sensor& sensor, SensorsPage& page)
    : SensorInfoWidget { sensor, page }
{
    auto layout = static_cast<QFormLayout*>(this->layout());

    auto const type = sensor.type();
    if (type) {
        auto label = make_qmanaged<QLabel>(*type);
        layout->addRow(QSL("Type:"), label.get());
    }

    if (auto const value = sensor.critical(); value) {
        auto label = make_qmanaged<QLabel>(QString::number(*value));
        layout->addRow(QSL("Critical:"), label.get());
    }

    if (auto const value = sensor.emergency(); value) {
        auto label = make_qmanaged<QLabel>(QString::number(*value));
        layout->addRow(QSL("Emergency:"), label.get());
    }

    if (auto const value = sensor.lowest(); value) {
        auto label = make_qmanaged<QLabel>(QString::number(*value));
        layout->addRow(QSL("Lowest:"), label.get());
    }

    if (auto const value = sensor.highest(); value) {
        auto label = make_qmanaged<QLabel>(QString::number(*value));
        layout->addRow(QSL("Highest:"), label.get());
    }
}

LmSensorsWidget::~LmSensorsWidget() = default;

} // namespace qsensors::dialog
