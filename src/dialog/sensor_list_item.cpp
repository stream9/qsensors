#include "sensor_list_item.hpp"

#include "core/sensor.hpp"

namespace qsensors::dialog {

SensorListItem::
SensorListItem(Sensor& s)
    : QListWidgetItem { s.label() }
    , m_sensor { s }
{}

SensorListItem::
SensorListItem(SensorListItem const& other)
    : QListWidgetItem { other }
    , m_sensor { other.m_sensor }
{}

SensorListItem::~SensorListItem() = default;

} // namespace qsensors::dialog
