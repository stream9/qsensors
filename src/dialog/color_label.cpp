#include "color_label.hpp"

#include <QColor>

namespace qsensors::dialog {

ColorLabel::
ColorLabel(QWidget& parent)
    : QLabel { &parent }
{
    this->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    this->setMinimumHeight(20);
    this->setFocusPolicy(Qt::NoFocus);
    this->setAutoFillBackground(true);
}

ColorLabel::~ColorLabel() = default;

QColor ColorLabel::
color() const
{
    return this->palette().color(QPalette::Window);
}

void ColorLabel::
setColor(QColor const& col)
{
    auto pal = this->palette();
    pal.setColor(QPalette::Window, col);

    this->setPalette(pal);
}

void ColorLabel::
mousePressEvent(QMouseEvent*)
{
    Q_EMIT pressed();
}

} // namespace qsensors::dialog
