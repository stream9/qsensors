#include "widget_page.hpp"

#include "color_label.hpp"
#include "gradient_label.hpp"
#include "sensor_list.hpp"
#include "settings_dialog.hpp"

#include "core/application.hpp"
#include "core/pointer.hpp"
#include "core/settings.hpp"

#include <QBoxLayout>
#include <QColorDialog>
#include <QComboBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QSlider>

namespace qsensors::dialog {

// WidgetPage

WidgetPage::
WidgetPage(SettingsDialog& dialog)
    : QWidget { &dialog }
    , m_dialog { dialog }
    , m_type { make_qmanaged<QComboBox>(this) }
    , m_lowColor { make_qmanaged<ColorLabel>(*this) }
    , m_midColor { make_qmanaged<ColorLabel>(*this) }
    , m_highColor { make_qmanaged<ColorLabel>(*this) }
    , m_preview { make_qmanaged<GradientLabel>(*this) }
    , m_center { make_qmanaged<QSlider>(Qt::Horizontal, this) }
    , m_sensors { make_qmanaged<SensorList>(dialog) }
{
    buildUI();

    load();

    connectUI();
}

WidgetPage::~WidgetPage() = default;

void WidgetPage::
save()
{
    auto& settings = m_dialog.application().settings();

    settings.beginGroup(QSL("widget"));

    settings.setValue(QSL("sensors"), m_sensors->ids());
    settings.setValue(QSL("lowColor"), m_lowColor->color().name());
    settings.setValue(QSL("midColor"), m_midColor->color().name());
    settings.setValue(QSL("highColor"), m_highColor->color().name());
    settings.setValue(QSL("center"), static_cast<double>(m_center->value()) / 100);
    settings.setValue(QSL("type"),
        m_type->currentText() == QSL("Bar") ? QSL("bar") : QSL("box"));

    settings.endGroup();
}

void WidgetPage::
buildUI()
{
    auto outer = make_qmanaged<QVBoxLayout>(this);
    outer->setContentsMargins(0, 0, 0, 0);

    //
    auto group1 = make_qmanaged<QGroupBox>(QSL("General"), this);
    auto layout1 = make_qmanaged<QHBoxLayout>(group1.get());

    auto layout1l = make_qmanaged<QFormLayout>();
    m_type->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    m_type->addItem(QSL("Box"));
    m_type->addItem(QSL("Bar"));
    layout1l->addRow(QSL("Type:"), m_type.get());

    m_lowColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout1l->addRow(QSL("Low Color:"), m_lowColor.get());

    m_midColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout1l->addRow(QSL("Middle Color:"), m_midColor.get());

    m_highColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout1l->addRow(QSL("High Color:"), m_highColor.get());

    layout1->addLayout(layout1l.get());

    outer->addWidget(group1.get());

    auto layout1r = make_qmanaged<QFormLayout>();
    m_preview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout1r->addRow(QSL("Preview:"), m_preview.get());

    m_center->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    m_center->setRange(0, 100);
    layout1r->addRow(QSL("Center:"), m_center.get());

    layout1->addLayout(layout1r.get());

    //
    auto group2 = make_qmanaged<QGroupBox>(QSL("Sensors"), this);
    auto layout2 = make_qmanaged<QVBoxLayout>(group2.get());

    layout2->addWidget(m_sensors.get());

    outer->addWidget(group2.get());
}

void WidgetPage::
connectUI()
{
    this->connect(m_lowColor.get(), &ColorLabel::pressed,
                  this,             &WidgetPage::onColorLabelPressed);
    this->connect(m_midColor.get(), &ColorLabel::pressed,
                  this,             &WidgetPage::onColorLabelPressed);
    this->connect(m_highColor.get(), &ColorLabel::pressed,
                  this,              &WidgetPage::onColorLabelPressed);
    this->connect(m_center.get(), &QSlider::sliderMoved,
                  this,           &WidgetPage::onCenterMoved);
    this->connect(m_type.get(), &QComboBox::currentTextChanged,
                  this,         &WidgetPage::onTypeChanged);
}

void WidgetPage::
load()
{
    auto& settings = m_dialog.application().settings();

    settings.beginGroup(QSL("widget"));

    auto const& ids = settings.value(QSL("sensors")).toStringList();
    auto const& lowColor = settings.value(QSL("lowColor")).toString();
    auto const& midColor = settings.value(QSL("midColor")).toString();
    auto const& highColor = settings.value(QSL("highColor")).toString();
    auto const center = settings.value(QSL("center")).toDouble();
    auto const& type = settings.value(QSL("type")).toString();

    settings.endGroup();

    m_sensors->setItems(ids);

    m_lowColor->setColor(lowColor);
    m_midColor->setColor(midColor);
    m_highColor->setColor(highColor);

    m_preview->setColor1(m_lowColor->color());
    m_preview->setColor2(m_midColor->color());
    m_preview->setColor3(m_highColor->color());

    m_preview->setCenter(center);
    m_center->setValue(static_cast<int>(center * 100));

    if (type == QSL("bar")) {
        m_type->setCurrentText(QSL("Bar"));
    }
    else {
        m_type->setCurrentText(QSL("Box"));
    }
}

void WidgetPage::
onColorLabelPressed()
{
    auto& label = to_ref(dynamic_cast<ColorLabel*>(this->sender()));

    auto const& color = QColorDialog::getColor(label.color(), this);

    if (color.isValid()) {
        label.setColor(color);

        m_dialog.setEdited();
    }
}

void WidgetPage::
onCenterMoved(int const value)
{
    assert(0 <= value && value <= 100);

    m_preview->setCenter(static_cast<double>(value) / 100);

    m_dialog.setEdited();
}

void WidgetPage::
onTypeChanged()
{
    m_dialog.setEdited();
}

} // namespace qsensors::dialog
