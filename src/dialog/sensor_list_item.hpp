#ifndef QSENSORS_DIALOG_SENSOR_LIST_ITEM_HPP
#define QSENSORS_DIALOG_SENSOR_LIST_ITEM_HPP

#include "core/sensor.hpp"

#include <QListWidgetItem>

class QListWidget;

namespace qsensors::dialog {

class SensorListItem : public QListWidgetItem
{
public:
    SensorListItem(Sensor&);
    SensorListItem(SensorListItem const&);
    ~SensorListItem();

    // accessor
    Sensor& sensor() const { return m_sensor; }

private:
    Sensor& m_sensor;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_SENSOR_LIST_ITEM_HPP
