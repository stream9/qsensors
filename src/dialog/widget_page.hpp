#ifndef QSENSORS_DIALOG_WIDGET_PAGE_HPP
#define QSENSORS_DIALOG_WIDGET_PAGE_HPP

#include "core/qmanaged_ptr.hpp"

#include <QWidget>

class QComboBox;
class QSlider;

namespace qsensors::dialog {

class ColorLabel;
class GradientLabel;
class SensorList;
class SettingsDialog;

class WidgetPage : public QWidget
{
    Q_OBJECT
public:
    WidgetPage(SettingsDialog&);
    ~WidgetPage() override;

    void save();

private:
    void buildUI();
    void connectUI();
    void load();

    Q_SLOT void onColorLabelPressed();
    Q_SLOT void onCenterMoved(int value);
    Q_SLOT void onTypeChanged();

private:
    SettingsDialog& m_dialog;

    qmanaged_ptr<QComboBox> m_type;

    qmanaged_ptr<ColorLabel> m_lowColor;
    qmanaged_ptr<ColorLabel> m_midColor;
    qmanaged_ptr<ColorLabel> m_highColor;

    qmanaged_ptr<GradientLabel> m_preview;
    qmanaged_ptr<QSlider> m_center;

    qmanaged_ptr<SensorList> m_sensors;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_WIDGET_PAGE_HPP
