#include "sensor_chooser.hpp"

#include "sensor_list_item.hpp"
#include "settings_dialog.hpp"

#include "core/application.hpp"
#include "core/pointer.hpp"

#include <QAction>
#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QIcon>
#include <QListWidget>
#include <QPushButton>
#include <QToolButton>

namespace qsensors::dialog {

static SensorListItem*
toSensorListItem(QListWidgetItem* const item)
{
    return dynamic_cast<SensorListItem*>(item);
}

static SensorListItem*
findItem(QListWidget const& list, Sensor const& sensor)
{
    for (auto row = 0; row < list.count(); ++row) {
        auto& item = to_ref(toSensorListItem(list.item(row)));

        if (&item.sensor() == &sensor) {
            return &item;
        }
    }

    return nullptr;
}

// SensorChooser

SensorChooser::
SensorChooser(SettingsDialog& dialog)
    : QDialog { &dialog }
    , m_dialog { dialog }
    , m_leftList { make_qmanaged<QListWidget>(this) }
    , m_rightList { make_qmanaged<QListWidget>(this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
    , m_moveRightAction { make_qmanaged<QAction>(this) }
    , m_moveLeftAction { make_qmanaged<QAction>(this) }
{
    buildActions();
    buildUI();
    fillLeftList();

    this->connect(m_leftList.get(), &QListWidget::currentItemChanged,
                  this,             &SensorChooser::updateActions);
    this->connect(m_leftList.get(),        &QListWidget::itemDoubleClicked,
                  m_moveRightAction.get(), &QAction::trigger);

    this->connect(m_rightList.get(), &QListWidget::currentItemChanged,
                  this,              &SensorChooser::updateActions);
    this->connect(m_rightList.get(),      &QListWidget::itemDoubleClicked,
                  m_moveLeftAction.get(), &QAction::trigger);

    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &QDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &QDialog::reject);

    updateActions();

    this->setWindowTitle(QSL("Choose Sensors"));
    this->resize(400, 300);
}

SensorChooser::~SensorChooser() = default;

void SensorChooser::
chooseSensors(QStringList const& ids)
{
    if (ids.isEmpty()) {
        chooseAll();
    }
    else {
        for (auto const& id: ids) {
            chooseItem(id);
        }
    }
}

void SensorChooser::
buildActions()
{
    auto& style = to_ref(this->style());

    m_moveRightAction->setIcon(style.standardIcon(QStyle::SP_ArrowRight));
    this->connect(m_moveRightAction.get(), &QAction::triggered,
                  this,                    &SensorChooser::moveRight);

    m_moveLeftAction->setIcon(style.standardIcon(QStyle::SP_ArrowLeft));
    this->connect(m_moveLeftAction.get(), &QAction::triggered,
                  this,                   &SensorChooser::moveLeft);
}

void SensorChooser::
buildUI()
{
    auto outer = make_qmanaged<QVBoxLayout>(this);

    auto layout1 = make_qmanaged<QHBoxLayout>();
    outer->addLayout(layout1.get());
    layout1->addWidget(m_leftList.get(), 1);

    //
    auto buttonLayout = make_qmanaged<QVBoxLayout>();
    layout1->addLayout(buttonLayout.get(), 0);

    buttonLayout->addStretch();

    auto moveRightButton = make_qmanaged<QToolButton>(this);
    moveRightButton->setDefaultAction(m_moveRightAction.get());
    buttonLayout->addWidget(moveRightButton.get());

    auto moveLeftButton = make_qmanaged<QToolButton>(this);
    moveLeftButton->setDefaultAction(m_moveLeftAction.get());
    buttonLayout->addWidget(moveLeftButton.get());

    buttonLayout->addStretch();

    //
    layout1->addWidget(m_rightList.get(), 1);

    auto& ok = to_ref(m_buttons->addButton(QDialogButtonBox::Ok));
    ok.setEnabled(false);
    m_buttons->addButton(QDialogButtonBox::Cancel);
    outer->addWidget(m_buttons.get());
}

void SensorChooser::
fillLeftList()
{
    auto& app = m_dialog.application();

    for (auto const& device: app.devices()) {
        for (auto& sensor: device.sensors()) {
            auto* const item = new SensorListItem { sensor };
            m_leftList->addItem(item);
        }
    }
}

QStringList SensorChooser::
choosedSensors()
{
    QStringList ids;

    for (auto row = 0; row < m_rightList->count(); ++row) {
        auto& item = to_ref(toSensorListItem(m_rightList->item(row)));
        ids.push_back(item.sensor().id());
    }

    return ids;
}

void SensorChooser::
chooseAll()
{
    for (auto row = 0; row < m_leftList->count(); ++row) {
        auto& item = to_ref(toSensorListItem(m_leftList->item(row)));

        chooseItem(item);
    }
}

void SensorChooser::
chooseItem(QString const& id)
{
    for (auto row = 0; row < m_leftList->count(); ++row) {
        auto& item = to_ref(toSensorListItem(m_leftList->item(row)));

        if (item.sensor().id() == id) {
            chooseItem(item);
        }
    }
}

void SensorChooser::
chooseItem(SensorListItem& item)
{
    item.setHidden(true);
    m_leftList->setCurrentItem(nullptr);

    auto* const copy = new SensorListItem { item };
    m_rightList->addItem(copy); // move ownership of copy to m_rightList
}

void SensorChooser::
moveRight()
{
    auto& item = to_ref(toSensorListItem(m_leftList->currentItem()));
    chooseItem(item);

    m_edited = true;

    updateActions();
}

void SensorChooser::
moveLeft()
{
    auto& rightItem = to_ref(toSensorListItem(m_rightList->currentItem()));

    auto& leftItem = to_ref(findItem(*m_leftList, rightItem.sensor()));
    leftItem.setHidden(false);

    auto const row = m_rightList->row(&rightItem);
    m_rightList->takeItem(row);
    delete &rightItem;

    m_edited = true;

    updateActions();
}

void SensorChooser::
updateActions()
{
    m_moveRightAction->setEnabled(m_leftList->currentItem() != nullptr);

    if (auto* const item = m_rightList->currentItem(); item != nullptr) {
        m_moveLeftAction->setEnabled(m_rightList->count() > 1);
    }
    else {
        m_moveLeftAction->setEnabled(false);
    }

    auto& ok = to_ref(m_buttons->button(QDialogButtonBox::Ok));
    ok.setEnabled(m_edited);
}

} // namespace qsensors::dialog
