#ifndef QSENSORS_DIALOG_SENSOR_LIST_HPP
#define QSENSORS_DIALOG_SENSOR_LIST_HPP

#include "core/fwd/sensor.hpp"
#include "core/qmanaged_ptr.hpp"

#include <QWidget>

class QAction;
class QListWidget;
class QListWidgetItem;
class QToolButton;

namespace qsensors::dialog {

class SettingsDialog;

class SensorList : public QWidget
{
    Q_OBJECT
public:
    SensorList(SettingsDialog&);
    ~SensorList() override;

    // query
    QStringList ids() const;
    Sensor* item(int index) const;
    Sensor* currentItem() const;
    int currentIndex() const;

    // modifier
    void setItems(QStringList const& ids);

    // signal
    Q_SIGNAL void currentItemChanged(int index) const;

private:
    void buildActions();
    void buildUI();

    Q_SLOT void onCurrentItemChanged(QListWidgetItem*, QListWidgetItem* prev);
    Q_SLOT void updateActions();
    Q_SLOT void moveUp();
    Q_SLOT void moveDown();
    Q_SLOT void openChooser();

private:
    SettingsDialog& m_dialog;
    qmanaged_ptr<QListWidget> m_list;
    qmanaged_ptr<QToolButton> m_chooseButton;

    qmanaged_ptr<QAction> m_moveUpAction;
    qmanaged_ptr<QAction> m_moveDownAction;
};

} // namespace qsensors::dialog


#endif // QSENSORS_DIALOG_SENSOR_LIST_HPP
