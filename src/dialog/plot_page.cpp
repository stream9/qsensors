#include "plot_page.hpp"

#include "color_label.hpp"
#include "sensor_list.hpp"
#include "settings_dialog.hpp"

#include "core/application.hpp"
#include "core/pointer.hpp"
#include "core/settings.hpp"
#include "plot/color_theme.hpp"

#include <QBoxLayout>
#include <QColorDialog>
#include <QComboBox>
#include <QEvent>
#include <QFormLayout>
#include <QGroupBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLabel>

namespace qsensors::dialog {

// PlotPage

PlotPage::
PlotPage(SettingsDialog& dialog)
    : QWidget { &dialog }
    , m_dialog { dialog }
    , m_colorTheme { make_qmanaged<QComboBox>(this) }
    , m_sensorList { make_qmanaged<SensorList>(dialog) }
    , m_graphColor { make_qmanaged<ColorLabel>(*this) }
{
    buildUI();
    load();
    connectUI();
    updateUI();
}

PlotPage::~PlotPage() = default;

void PlotPage::
save()
{
    auto& settings = m_dialog.application().settings();

    settings.beginGroup(QSL("plot"));

    settings.setValue(QSL("sensors"), m_sensorList->ids());
    settings.setValue(QSL("theme"), m_colorTheme->currentText());

    if (!m_colorMap.empty()) {
        QJsonObject colorMap;
        for (auto const& [sensor, color]: m_colorMap) {
            colorMap[sensor->id()] = color.name();
        }
        QJsonDocument const doc { colorMap };
        auto const& json = doc.toJson(QJsonDocument::Compact);
        settings.setValue(QSL("graphColors"), QString(json));
    }
    else {
        settings.remove(QSL("graphColors"));
    }

    settings.endGroup();
}

void PlotPage::
buildUI()
{
    auto outer = make_qmanaged<QVBoxLayout>(this);
    outer->setContentsMargins(0, 0, 0, 0);

    // General
    auto group1 = make_qmanaged<QGroupBox>(this);
    group1->setTitle(QSL("General"));

    auto layout1 = make_qmanaged<QFormLayout>(group1.get());

    m_colorTheme->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    for (auto const& name: plot::themeNames()) {
        m_colorTheme->addItem(name);
    }
    layout1->addRow(QSL("Color Theme:"), m_colorTheme.get());

    outer->addWidget(group1.get());

    // Sensors
    auto group2 = make_qmanaged<QGroupBox>(this);
    group2->setTitle(QSL("Sensors"));

    auto layout2 = make_qmanaged<QHBoxLayout>(group2.get());

    layout2->addWidget(m_sensorList.get());

    auto layout3 = make_qmanaged<QFormLayout>();

    m_graphColor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout3->addRow(QSL("Graph Color:"), m_graphColor.get());

    layout2->addLayout(layout3.get(), 1);

    outer->addWidget(group2.get());
}

void PlotPage::
connectUI()
{
    this->connect(m_colorTheme.get(), &QComboBox::currentIndexChanged,
                  this,               &PlotPage::onColorThemeChanged);

    this->connect(m_sensorList.get(), &SensorList::currentItemChanged,
                  this,               &PlotPage::updateGraphColor);

    this->connect(m_graphColor.get(), &ColorLabel::pressed,
                  this,               &PlotPage::onGraphColorPressed);
}

void PlotPage::
updateUI()
{
    m_graphColor->setEnabled(m_sensorList->currentItem() != nullptr);
}

void PlotPage::
load()
{
    auto& settings = m_dialog.application().settings();

    settings.beginGroup(QSL("plot"));

    auto const& ids = settings.value(QSL("sensors")).toStringList();
    auto const& theme = settings.value(QSL("theme")).toString();
    auto const& colorMap = settings.value(QSL("graphColors")).toString();

    settings.endGroup();

    m_colorTheme->setCurrentText(theme);
    m_sensorList->setItems(ids);

    QJsonParseError err;
    auto const& doc = QJsonDocument::fromJson(colorMap.toUtf8(), &err);
    if (err.error == QJsonParseError::NoError && doc.isObject()) {
        auto const& obj = doc.object();
        for (auto it = obj.begin(); it != obj.end(); ++it) {
            auto const& id = it.key();
            auto* const sensor = m_dialog.application().sensor(id);
            if (sensor == nullptr) {
                qWarning() << "plot.graphColors: unknown sensor ID:" << id;
                continue;
            }

            auto const& value = it.value();
            if (!value.isString()) {
                qWarning() << "plot.graphColors: invalid value:" << value;
                continue;
            }
            auto const& colorName = value.toString();

            QColor const color { colorName };
            if (!color.isValid()) {
                qWarning() << "plot.graphColors: invalid color name:" << colorName;
                continue;
            }

            m_colorMap[sensor] = color;
        }
    }
}

void PlotPage::
onColorThemeChanged()
{
    m_colorMap.clear();

    updateGraphColor(m_sensorList->currentIndex());

    m_dialog.setEdited(true);
}

void PlotPage::
onGraphColorPressed()
{
    auto const& color = QColorDialog::getColor(
        m_graphColor->color(),
        this
    );

    if (color.isValid()) {
        m_graphColor->setColor(color);

        auto& sensor = to_ref(m_sensorList->currentItem());
        m_colorMap[&sensor] = color;

        m_dialog.setEdited();
    }
}

void PlotPage::
updateGraphColor(int const index)
{
    auto* const sensor = m_sensorList->item(index);
    if (sensor) {
        auto theme = plot::makeColorTheme(m_colorTheme->currentText());
        auto col = theme->graphColor(index);

        auto const it = m_colorMap.find(sensor);
        if (it != m_colorMap.end()) {
            col = it->second;
        }

        m_graphColor->setColor(col);
    }

    updateUI();
}

} // namespace qsensors::dialog
