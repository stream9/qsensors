#ifndef QSENSORS_DIALOG_GRADIENT_LABEL_HPP
#define QSENSORS_DIALOG_GRADIENT_LABEL_HPP

#include <QColor>
#include <QLabel>

class QMouseEvent;
class QResizeEvent;
class QWidget;

namespace qsensors::dialog {

class GradientLabel : public QLabel
{
    Q_OBJECT
public:
    GradientLabel(QWidget& parent);
    ~GradientLabel() override;

    // modifier
    void setColor1(QColor const&);
    void setColor2(QColor const&);
    void setColor3(QColor const&);
    void setCenter(double v); // 0 <= v && v <= 1.0

    // signal
    Q_SIGNAL void pressed() const;

protected:
    // override QWidget
    void mousePressEvent(QMouseEvent*) override;
    void resizeEvent(QResizeEvent*) override;

    void updateGradient();

private:
    QColor m_col1, m_col2, m_col3;
    double m_center = 0.5;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_GRADIENT_LABEL_HPP
