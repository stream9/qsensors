#ifndef QSENSORS_DIALOG_SETTINGS_DIALOG_HPP
#define QSENSORS_DIALOG_SETTINGS_DIALOG_HPP

#include "core/fwd/application.hpp"
#include "core/qmanaged_ptr.hpp"

#include <QDialog>

class QDialogButtonBox;
class QWidget;

namespace qsensors::dialog {

class WidgetPage;
class PlotPage;
class SensorsPage;

class SettingsDialog : public QDialog
{
public:
    SettingsDialog(Application&, QWidget* parent = nullptr);
    ~SettingsDialog() noexcept;

    // accessor
    Application& application() { return m_app; }
    bool edited() const { return m_edited; }

    // modifier
    void setEdited(bool v = true);

private:
    Q_SLOT void save() const;

private:
    Application& m_app;
    qmanaged_ptr<SensorsPage> m_sensorsPage;
    qmanaged_ptr<WidgetPage> m_widgetPage;
    qmanaged_ptr<PlotPage> m_plotPage;
    qmanaged_ptr<QDialogButtonBox> m_buttons;
    bool m_edited = false;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_SETTINGS_DIALOG_HPP
