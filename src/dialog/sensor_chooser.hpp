#ifndef QSENSORS_DIALOG_SENSOR_CHOOSER_HPP
#define QSENSORS_DIALOG_SENSOR_CHOOSER_HPP

#include "core/qmanaged_ptr.hpp"
#include "core/fwd/sensor.hpp"

#include <QDialog>
#include <QString>

class QAction;
class QDialogButtonBox;
class QListWidget;

namespace qsensors::dialog {

class SensorListItem;
class SettingsDialog;

class SensorChooser : public QDialog
{
    Q_OBJECT
public:
    SensorChooser(SettingsDialog&);
    ~SensorChooser() override;

    // query
    QStringList choosedSensors();

    // command
    void chooseSensors(QStringList const& ids);

private:
    void buildActions();
    void buildUI();
    void fillLeftList();
    void chooseAll();
    void chooseItem(SensorListItem&);
    void chooseItem(QString const& id);

    Q_SLOT void moveRight();
    Q_SLOT void moveLeft();
    Q_SLOT void updateActions();

private:
    SettingsDialog& m_dialog;
    qmanaged_ptr<QListWidget> m_leftList;
    qmanaged_ptr<QListWidget> m_rightList;
    qmanaged_ptr<QDialogButtonBox> m_buttons;

    qmanaged_ptr<QAction> m_moveRightAction;
    qmanaged_ptr<QAction> m_moveLeftAction;

    bool m_edited = false;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_SENSOR_CHOOSER_HPP
