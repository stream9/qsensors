#ifndef QSENSORS_DIALOG_SENSORS_PAGE_HPP
#define QSENSORS_DIALOG_SENSORS_PAGE_HPP

#include "core/fwd/sensor.hpp"
#include "core/fwd/settings.hpp"
#include "core/qmanaged_ptr.hpp"
#include "udisks/fwd/sensor.hpp"
#include "lm_sensors/fwd/sensor.hpp"

#include <QWidget>
#include <QListWidgetItem>

class QCheckBox;
class QLabel;
class QLineWidget;
class QListWidget;
class QShowEvent;
class QStackedLayout;

namespace qsensors::dialog {

class SettingsDialog;
class SensorInfoWidget;

class SensorsPage : public QWidget
{
    Q_OBJECT
public:
    SensorsPage(SettingsDialog&);
    ~SensorsPage() override;

    // accessor
    SettingsDialog& dialog() const { return m_dialog; }

    // command
    void save() const;

private:
    void loadSensors();
    SensorInfoWidget& makeInfoWidget(Sensor&);
    QStackedLayout& detailLayout() const;

    Q_SLOT void onCurrentChanged(QListWidgetItem* current, QListWidgetItem* prev);

private:
    SettingsDialog& m_dialog;
    qmanaged_ptr<QListWidget> m_list;
    qmanaged_ptr<QWidget> m_detail;
};

class SensorItem : public QListWidgetItem
{
public:
    SensorItem(Sensor&, QListWidget& parent);
    ~SensorItem() override;

    // accessor
    Sensor& sensor() const { return m_sensor; }

private:
    Sensor& m_sensor;
};

class SensorInfoWidget : public QWidget
{
    Q_OBJECT
public:
    SensorInfoWidget(Sensor&, SensorsPage&);
    ~SensorInfoWidget() override;

    // accessor
    Sensor& sensor() const { return m_sensor; }

    // query
    QString label() const;

private:
    Q_SLOT void onEdited() const;

private:
    SensorsPage& m_page;
    Sensor& m_sensor;
    qmanaged_ptr<QLineEdit> m_label;
    qmanaged_ptr<QLabel> m_id;
    qmanaged_ptr<QLabel> m_source;
    qmanaged_ptr<QLabel> m_name;
    qmanaged_ptr<QLabel> m_minimum;
    qmanaged_ptr<QLabel> m_maximum;
};

class AtaWidget : public SensorInfoWidget
{
    Q_OBJECT
public:
    AtaWidget(udisks::Sensor&, SensorsPage&);
    ~AtaWidget() override;
};

class LmSensorsWidget : public SensorInfoWidget
{
    Q_OBJECT
public:
    LmSensorsWidget(lm_sensors::Sensor&, SensorsPage&);
    ~LmSensorsWidget() override;
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_SENSORS_PAGE_HPP
