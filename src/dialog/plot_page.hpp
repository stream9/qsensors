#ifndef QSENSORS_DIALOG_PLOT_PAGE_HPP
#define QSENSORS_DIALOG_PLOT_PAGE_HPP

#include "core/fwd/sensor.hpp"
#include "core/qmanaged_ptr.hpp"

#include <boost/container/flat_map.hpp>

#include <QWidget>

class QComboBox;

namespace qsensors::dialog {

class ColorLabel;
class SensorList;
class SettingsDialog;

class PlotPage : public QWidget
{
    Q_OBJECT
public:
    PlotPage(SettingsDialog&);
    ~PlotPage() override;

    void save();

private:
    void buildUI();
    void connectUI();
    void updateUI();

    void load();

    Q_SLOT void onColorThemeChanged();
    Q_SLOT void onGraphColorPressed();
    Q_SLOT void updateGraphColor(int index);

private:
    SettingsDialog& m_dialog;

    qmanaged_ptr<QComboBox> m_colorTheme;
    qmanaged_ptr<SensorList> m_sensorList;
    qmanaged_ptr<ColorLabel> m_graphColor;

    boost::container::flat_map<Sensor*, QColor> m_colorMap; // non-null
};

} // namespace qsensors::dialog

#endif // QSENSORS_DIALOG_PLOT_PAGE_HPP
