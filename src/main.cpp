#include "core/application.hpp"
#include "core/qmanaged_ptr.hpp"
#include "core/global.hpp"
#include "core/pointer.hpp"
#include "plot/widget.hpp"
#include "widget/widget.hpp"

#include <config.h>

#include <QAction>
#include <QApplication>
#include <QMouseEvent>
#include <QPointer>
#include <QVBoxLayout>
#include <QWidget>

using namespace qsensors;

class Main : public QWidget
{
    Q_OBJECT
public:
    Main()
    {
        auto layout = make_qmanaged<QVBoxLayout>(this);
        layout->setContentsMargins(0, 0, 0, 0);

        auto widget = make_qmanaged<widget::Widget>(m_app);
        layout->addWidget(widget.get());

        auto action = make_qmanaged<QAction>(QSL("Configure"), this);
        action->setShortcut(Qt::CTRL | Qt::Key_P);
        this->connect(action.get(), &QAction::triggered,
                      this,         &Main::showSettingsDialog);
        this->addAction(action.get());

        this->setContextMenuPolicy(Qt::ActionsContextMenu);
    }

    ~Main() override = default;

protected:
    void mousePressEvent(QMouseEvent* const e) override
    {
        auto& ev = to_ref(e);

        if (ev.button() != Qt::LeftButton) return;

        if (m_plot.isNull()) {
            m_plot = new plot::Widget { m_app };
            m_plot->setAttribute(Qt::WA_DeleteOnClose);
        }

        m_plot->show();
    }

private:
    Q_SLOT void showSettingsDialog()
    {
        m_app.showSettingsDialog(this);
    }

private:
    Application m_app;
    QPointer<plot::Widget> m_plot;
};

int main(int argc, char *argv[])
{
    QApplication app { argc, argv };

    app.setApplicationVersion(QSENSOR_VERSION);

    Main main;
    main.show();

    return app.exec();
}

#include "main.moc"
