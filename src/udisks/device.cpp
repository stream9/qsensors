#include "device.hpp"

#include "sensor.hpp"

#include "core/global.hpp"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>
#include <QXmlStreamReader>

namespace qsensors::udisks {

static void
skipFirstNode(QXmlStreamReader& xml)
{
    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement() && xml.name() == QSL("node")) {
            return;
        }
    }
}

static std::vector<QString>
getDrives()
{
    std::vector<QString> rv;

    QDBusInterface interface {
        QSL("org.freedesktop.UDisks2"),
        QSL("/org/freedesktop/UDisks2/drives"),
        QSL("org.freedesktop.DBus.Introspectable"),
        QDBusConnection::systemBus()
    };
    if (!interface.isValid()) return rv;

    QDBusReply<QString> reply = interface.call(QSL("Introspect"));
    if (!reply.isValid()) return rv;

    QXmlStreamReader xml { reply.value() };
    skipFirstNode(xml);

    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement() && xml.name() == QSL("node")) {
            rv.push_back(xml.attributes().value(QSL("name")).toString());
        }
    }

    return rv;
}

Device::
Device()
{
    for (auto const& name: getDrives()) {
        this->addSensor(std::make_unique<Sensor>(name));
    }
}

} // namespace qsensors::udisks
