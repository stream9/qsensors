#ifndef QSENSORS_UDISKS_DEVICE_HPP
#define QSENSORS_UDISKS_DEVICE_HPP

#include "core/device.hpp"

namespace qsensors::udisks {

class Device : public qsensors::Device
{
public:
    Device();
};

} // namespace qsensors::udisks

#endif // QSENSORS_UDISKS_DEVICE_HPP
