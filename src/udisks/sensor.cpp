#include "sensor.hpp"

#include "core/global.hpp"

#include <QCryptographicHash>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QString>
#include <QVariant>

namespace qsensors::udisks {

static double
toCelsius(double const k)
{
    return k - 273.15;
}

static QString
generateId(QString const& name)
{
    return QCryptographicHash::hash(
        QSL("udisks_%1").arg(name).toUtf8(),
        QCryptographicHash::Md5
    ).toHex();
}

// Sensor

Sensor::
Sensor(QString const& name)
    : qsensors::Sensor { generateId(name) }
    , m_dbusPath { QSL("/org/freedesktop/UDisks2/drives/%1").arg(name) }
{
    this->setSource(QSL("UDisks2"));
    this->setName(name);
    this->setMinimum(20);
    this->setMaximum(50);
    update();
}

Sensor::~Sensor() = default;

void Sensor::
update()
{
    QDBusInterface iface {
        QSL("org.freedesktop.UDisks2"),
        m_dbusPath,
        QSL("org.freedesktop.UDisks2.Drive.Ata"),
        QDBusConnection::systemBus()
    };
    if (!iface.isValid()) {
        this->setValue(std::nullopt);
        return;
    }

    auto const& prop = iface.property("SmartTemperature");
    if (!prop.isValid()) {
        this->setValue(std::nullopt);
        return;
    }

    this->setValue(toCelsius(prop.toDouble()));
}

} // namespace qsensors::udisks
