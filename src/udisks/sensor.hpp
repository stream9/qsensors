#ifndef QSENSORS_UDISKS_SENSOR_HPP
#define QSENSORS_UDISKS_SENSOR_HPP

#include "core/sensor.hpp"

class QString;

namespace qsensors::udisks {

class Sensor : public qsensors::Sensor
{
    Q_OBJECT
public:
    Sensor(QString const& name);
    ~Sensor() override;

    // command
    void update() override;

private:
    QString m_dbusPath;
};

} // namespace qsensors::udisks

#endif // QSENSORS_UDISKS_SENSOR_HPP
