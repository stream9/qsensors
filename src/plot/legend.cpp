#include "legend.hpp"

#include "plot.hpp"

#include "core/pointer.hpp"

#include <QColor>
#include <QSignalBlocker>
#include <QStringList>
#include <QTreeWidgetItem>

namespace qsensors::plot {

static void
setCurrent(QTreeWidgetItem& item, double const value)
{
    item.setText(1, QString::number(value));
}

static void
setMinimum(QTreeWidgetItem& item, double const value)
{
    item.setText(2, QString::number(value));
}

static void
setMaximum(QTreeWidgetItem& item, double const value)
{
    item.setText(3, QString::number(value));
}

// Legend

Legend::
Legend(Plot& plot, QWidget& parent)
    : QTreeWidget { &parent }
    , m_plot { plot }
{
    QStringList headers { "Name", "Current", "Min", "Max" };
    this->setHeaderLabels(headers);
    this->header()->resizeSections(QHeaderView::ResizeToContents);
    this->header()->resizeSection(0, 100);

    this->setRootIsDecorated(false);
    this->setMouseTracking(true);
    this->setSelectionMode(QTreeWidget::NoSelection);
    this->setAllColumnsShowFocus(true);

    this->connect(this, &QTreeWidget::itemChanged,
                  this, &Legend::onItemChanged);
    this->connect(this, &QTreeWidget::currentItemChanged,
                  this, &Legend::onCurrentItemChanged);

    this->connect(&m_plot, &Plot::graphHovered,
                  this,    &Legend::selectSensor);
    this->connect(&m_plot, &Plot::graphLeaved,
                  this,    &Legend::unselectSensor);
    this->connect(&m_plot, &Plot::graphColorChanged,
                  this,    &Legend::onGraphColorChanged);

    this->setCurrentItem(nullptr);
}

Legend::~Legend() = default;

void Legend::
addSensor(Sensor const& sensor)
{
    auto const value = sensor.value().value_or(0);
    auto const& range = sensor.range();

    QStringList columns;
    columns.push_back(sensor.label());
    columns.push_back(QString::number(value));
    columns.push_back(QString::number(range.lower));
    columns.push_back(QString::number(range.upper));

    auto* const item = new QTreeWidgetItem { this, columns };
    item->setCheckState(0, Qt::Checked);
    this->addTopLevelItem(item);

    m_sensors.push_back(&sensor);
}

void Legend::
clear()
{
    QTreeWidget::clear();
    m_sensors.clear();
}

void Legend::
update()
{
    for (size_t i = 0; i < m_sensors.size(); ++i) {
        auto& item = to_ref(this->topLevelItem(static_cast<int>(i)));
        auto& sensor = to_ref(m_sensors.at(i));
        auto const value = sensor.value().value_or(0);
        auto const& range = sensor.range();

        setCurrent(item, value);
        setMinimum(item, range.lower);
        setMaximum(item, range.upper);
    }
}

void Legend::
mouseMoveEvent(QMouseEvent* const ev)
{
    QTreeWidget::mouseMoveEvent(ev);

    if (ev == nullptr) return;

    auto* item = this->itemAt(ev->pos());
    if (item == nullptr) {
        m_plot.unselectGraph();
    }
    else {
        this->setCurrentItem(item);
    }
}

void Legend::
leaveEvent(QEvent*)
{
    m_plot.unselectGraph();
}

void Legend::
onItemChanged(QTreeWidgetItem* const item, int const column)
{
    if (item == nullptr) return;
    if (column != 0) return;

    m_plot.setGraphVisible(this->indexOfTopLevelItem(item),
                           item->checkState(column) == Qt::Checked );
}

void Legend::
onCurrentItemChanged(QTreeWidgetItem* const current, QTreeWidgetItem*)
{
    if (current == nullptr) return;

    m_plot.selectGraph(this->indexOfTopLevelItem(current));
}

void Legend::
selectSensor(int const index)
{
    QSignalBlocker blocker { this };

    auto& item = to_ref(this->topLevelItem(index));
    this->setCurrentItem(&item);
}

void Legend::
unselectSensor()
{
    QSignalBlocker blocker { this };

    this->setCurrentItem(nullptr);
}

void Legend::
onGraphColorChanged(int const index, QColor const& color)
{
    auto* const item = this->topLevelItem(index);
    if (!item) return;

    item->setForeground(0, color);
}

} // namespace qsensors::plot
