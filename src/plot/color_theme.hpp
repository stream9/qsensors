#ifndef QSENSORS_PLOT_COLOR_THEME_HPP
#define QSENSORS_PLOT_COLOR_THEME_HPP

#include "core/global.hpp"

#include <QBrush>
#include <QPen>
#include <QColor>

#include <memory>
#include <vector>

namespace qsensors::plot {

class ColorTheme
{
public:
    virtual ~ColorTheme() {}

    // query
    virtual QBrush const& backgroundBrush() const = 0;
    virtual QPen const& axisPen() const = 0;
    virtual QColor const& tickLabelColor() const = 0;
    virtual QColor const& graphColor(int index) const = 0;
};

class ColorThemeImpl : public ColorTheme
{
public:
    ColorThemeImpl() = default;
    ~ColorThemeImpl() override = default;

    // accessor
    auto& backgroundBrush() { return m_backgroundBrush; }
    auto& axisPen() { return m_axisPen; }
    auto& tickLabelColor() { return m_tickLabelColor; }
    auto& graphColors() { return m_graphColors; }

    // query
    QBrush const& backgroundBrush() const override { return m_backgroundBrush; }
    QPen const& axisPen() const override { return m_axisPen; }
    QColor const& tickLabelColor() const override { return m_tickLabelColor; }
    QColor const& graphColor(int index) const override
    {
        return m_graphColors[static_cast<size_t>(index) % m_graphColors.size()];
    }

private:
    QBrush m_backgroundBrush;
    QPen m_axisPen;
    QColor m_tickLabelColor;
    std::vector<QColor> m_graphColors;
};

class DefaultTheme : public ColorThemeImpl
{
public:
    DefaultTheme();

    // query
    static QString name() { return QSL("Default"); }
};

class DarkTheme : public ColorThemeImpl
{
public:
    DarkTheme();

    // query
    static QString name() { return QSL("Dark"); }
};

class LightTheme : public ColorThemeImpl
{
public:
    LightTheme();

    // query
    static QString name() { return QSL("Light"); }
};

class BlueCeruleanTheme : public ColorThemeImpl
{
public:
    BlueCeruleanTheme();

    // query
    static QString name() { return QSL("Blue Cerulean"); }
};

class BlueIcyTheme : public ColorThemeImpl
{
public:
    BlueIcyTheme();

    // query
    static QString name() { return QSL("Blue Icy"); }
};

class BlueNcsTheme : public ColorThemeImpl
{
public:
    BlueNcsTheme();

    // query
    static QString name() { return QSL("Blue Ncs"); }
};

class BrownSandTheme : public ColorThemeImpl
{
public:
    BrownSandTheme();

    // query
    static QString name() { return QSL("Brown Sand"); }
};

class HighContrastTheme : public ColorThemeImpl
{
public:
    HighContrastTheme();

    // query
    static QString name() { return QSL("High Contrast"); }
};

std::unique_ptr<ColorTheme> makeColorTheme(QString const& name);

std::array<QString, 8> themeNames();

} // namespace qsensors::plot

#endif // QSENSORS_PLOT_COLOR_THEME_HPP
