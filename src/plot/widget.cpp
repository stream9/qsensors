#include "widget.hpp"

#include "plot.hpp"
#include "legend.hpp"

#include "core/application.hpp"
#include "core/global.hpp"
#include "core/pointer.hpp"
#include "core/sensor.hpp"
#include "core/sensor_range.hpp"
#include "core/settings.hpp"

#include <QHBoxLayout>
#include <QSplitter>

namespace qsensors::plot {

Widget::
Widget(Application& app, QWidget* const parent)
    : QWidget { parent }
    , m_app { app }
    , m_splitter { make_qmanaged<QSplitter>(Qt::Horizontal, this) }
    , m_plot { make_qmanaged<Plot>(*this) }
    , m_legend { make_qmanaged<Legend>(*m_plot, *this) }
{
    this->setWindowTitle(QSL("QSensors"));

    m_splitter->addWidget(m_legend.get());
    m_splitter->setStretchFactor(0, 0);
    m_splitter->addWidget(m_plot.get());
    m_splitter->setStretchFactor(1, 1);

    auto layout = make_qmanaged<QHBoxLayout>(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_splitter.get());

    this->connect(&m_app, &Application::updated,
                  this,   &Widget::update);
    this->connect(&m_app, &Application::settingChanged,
                  this,   &Widget::reloadSettings);

    auto action = make_qmanaged<QAction>(QSL("Configure"), this);
    action->setShortcut(Qt::CTRL | Qt::Key_P);
    this->connect(action.get(), &QAction::triggered,
                  this,         &Widget::showSettingsDialog);
    this->addAction(action.get());

    this->setContextMenuPolicy(Qt::ActionsContextMenu);
    this->resize(800, 250);

    loadSettings();

    loadState();
}

Widget::~Widget() = default;

void Widget::
closeEvent(QCloseEvent*)
{
    saveState();
}

void Widget::
reloadSettings()
{
    m_plot->clear();
    m_legend->clear();

    loadSettings();
}

void Widget::
update()
{
    m_plot->update();
    m_legend->update();
}

void Widget::
showSettingsDialog()
{
    m_app.showSettingsDialog(this);
}

void Widget::
loadSettings()
{
    auto& settings = m_app.settings();

    settings.beginGroup(QSL("plot"));

    auto const& ids = settings.value(QSL("sensors")).toStringList();
    auto const& theme = settings.value(QSL("theme")).toString();
    auto const& graphColors = settings.value(QSL("graphColors")).toString();
    auto duration = settings.value(QSL("duration")).toInt();

    settings.endGroup();

    addSensors(ids);

    m_plot->setColorTheme(theme);
    m_plot->setDuration(duration);

    setGraphColors(graphColors);
}

void Widget::
addSensors(QStringList const& ids)
{
    if (ids.isEmpty()) {
        for (auto const& sensor: m_app.sensors()) {
            addSensor(sensor);
        }
    }
    else {
        for (auto const& id: ids) {
            auto* const sensor = m_app.sensor(id);
            if (!sensor) continue;

            addSensor(*sensor);
        }
    }
}

void Widget::
addSensor(Sensor const& sensor)
{
    auto const& history = m_app.history();

    m_plot->addSensor(sensor, history);
    m_legend->addSensor(sensor);
}

void Widget::
setGraphColors(QString const& json)
{
    if (json.isEmpty()) return;

    QJsonParseError err;
    auto const& doc = QJsonDocument::fromJson(json.toUtf8(), &err);
    if (err.error != QJsonParseError::NoError || !doc.isObject()) {
        qWarning() << "plot.graphColor: invalid value: " << json;
        return;
    }
    auto const& obj = doc.object();

    for (int i = 0; i < m_plot->graphCount(); ++i) {
        auto& sensor = to_ref(m_plot->sensor(i));

        auto const it = obj.find(sensor.id());
        if (it == obj.end()) continue;

        auto const& name = it.value().toString();
        QColor const col { name };
        if (!col.isValid()) continue;

        m_plot->setGraphColor(i, col);
    }
}

void Widget::
loadState()
{
    auto& settings = m_app.settings();

    settings.beginGroup(QSL("plot"));

    auto size = settings.value(QSL("size"));
    if (size.isValid()) {
        this->resize(size.toSize());
    }

    auto pos = settings.value(QSL("pos"));
    if (pos.isValid()) {
        this->move(pos.toPoint());
    }

    m_splitter->restoreState(
        settings.value(QSL("splitterState")).toByteArray() );

    auto width = settings.value(QSL("width"));
    if (width.isValid()) {
        m_plot->setWidth(width.toInt());
    }

    settings.endGroup();
}

void Widget::
saveState() const
{
    auto& settings = m_app.settings();

    settings.beginGroup(QSL("plot"));

    settings.setValue(QSL("size"), this->size());
    settings.setValue(QSL("pos"), this->pos());
    settings.setValue(QSL("splitterState"), m_splitter->saveState());
    settings.setValue(QSL("width"), m_plot->width());

    settings.endGroup();
}

} // namespace qsensors::plot
