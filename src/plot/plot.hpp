#ifndef QSENSORS_PLOT_PLOT_HPP
#define QSENSORS_PLOT_PLOT_HPP

#include "core/fwd/history.hpp"
#include "core/fwd/sensor.hpp"
#include "core/fwd/settings.hpp"

#include <memory>

#include <qcustomplot.h>

#include <boost/container/flat_map.hpp>

class QColor;
class QMouseEvent;
class QWidget;
class QWheelEvent;

class QCPGraph;

namespace qsensors::plot {

class ColorTheme;

class Plot : public QCustomPlot
{
    Q_OBJECT
public:
    static constexpr int minWidth = 60;
    static constexpr int minDuration = 250;

public:
    Plot(QWidget& parent);
    ~Plot() override;

    // query
    ColorTheme const& colorTheme() const { return *m_theme; }
    QColor graphColor(int index) const;
    Sensor const* sensor(int index) const;
    int width() const noexcept;

    // modifier
    void setGraphVisible(int index, bool);
    void setGraphColor(int index, QColor const&);
    void setColorTheme(QString const& name);
    void setDuration(int d) noexcept { m_duration = d < minDuration ? minDuration : d; }
    void setWidth(int w) noexcept;
    void clear();

    // command
    void addSensor(Sensor const&);
    void addSensor(Sensor const&, History const&);
    void update();
    void selectGraph(int index);
    void unselectGraph();

    // signal
    Q_SIGNAL void graphHovered(int index) const;
    Q_SIGNAL void graphLeaved() const;
    Q_SIGNAL void graphColorChanged(int index, QColor const&);

protected:
    void wheelEvent(QWheelEvent*) override;

private:
    void setUpColors();
    void rescale();
    void selectGraph(QCPGraph&);

    Q_SLOT void onMouseMove(QMouseEvent*);

private:
    boost::container::flat_map<Sensor const*, QCPGraph*> m_graphs; // non-null
    std::unique_ptr<ColorTheme> m_theme; // non-null
    int m_duration = 250;
    int m_width = 600;
    bool m_hovered = false;
};

} // namespace qsensors::plot

#endif // QSENSORS_PLOT_PLOT_HPP
