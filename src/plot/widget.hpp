#ifndef QSENSORS_PLOT_WIDGET_HPP
#define QSENSORS_PLOT_WIDGET_HPP

#include "core/fwd/application.hpp"
#include "core/fwd/sensor.hpp"
#include "core/qmanaged_ptr.hpp"

#include <QTimer>
#include <QWidget>

class QSplitter;
class QCloseEvent;

namespace qsensors::plot {

class Legend;
class Plot;

class Widget : public QWidget
{
public:
    Widget(Application&, QWidget* parent = nullptr);
    ~Widget() override;

protected:
    void closeEvent(QCloseEvent*) override;

private:
    Q_SLOT void reloadSettings();
    Q_SLOT void update();
    Q_SLOT void showSettingsDialog();

    void loadSettings();
    void loadState();
    void saveState() const;
    void addSensors(QStringList const& ids);
    void addSensor(Sensor const&);
    void setGraphColors(QString const& json);

private:
    Application& m_app;
    qmanaged_ptr<QSplitter> m_splitter;
    qmanaged_ptr<Plot> m_plot;
    qmanaged_ptr<Legend> m_legend;
};

} // namespace qsensors::plot

#endif // QSENSORS_PLOT_WIDGET_HPP
