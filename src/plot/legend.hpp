#ifndef QSENSORS_PLOT_LEGEND_HPP
#define QSENSORS_PLOT_LEGEND_HPP

#include "core/sensor.hpp"

#include <vector>

#include <QTreeWidget>

class QEvent;
class QMouseEvent;
class QTreeWidgetItem;

namespace qsensors::plot {

class Plot;

class Legend : public QTreeWidget
{
public:
    Legend(Plot&, QWidget& parent);
    ~Legend() noexcept;

    // modifier
    void addSensor(Sensor const&);
    void clear();

    // command
    void update();

protected:
    void mouseMoveEvent(QMouseEvent*) override;
    void leaveEvent(QEvent*) override;

private:
    Q_SLOT void onItemChanged(QTreeWidgetItem*, int column);
    Q_SLOT void onCurrentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);
    Q_SLOT void selectSensor(int index);
    Q_SLOT void unselectSensor();
    Q_SLOT void onGraphColorChanged(int index, QColor const&);

private:
    Plot& m_plot;
    std::vector<Sensor const*> m_sensors; // non-null
};

} // namespace qsensors::plot

#endif // QSENSORS_PLOT_LEGEND_HPP
