#include "color_theme.hpp"

namespace qsensors::plot {

// DefaultTheme

DefaultTheme::
DefaultTheme()
{
    QLinearGradient g { 0.5, 0.0, 0.5, 1.0 };
    g.setColorAt(0.0, QRgb(0xffffff));
    g.setColorAt(1.0, QRgb(0xffffff));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0xd7d6d5) };
    tickLabelColor() = QRgb(0x35322f);

    graphColors().reserve(8);
    graphColors().emplace_back(0x80c342);
    graphColors().emplace_back(0x328930);
    graphColors().emplace_back(0x006325);
    graphColors().emplace_back(0x35322f);
    graphColors().emplace_back(0x5d5b59);
    graphColors().emplace_back(0x868482);
    graphColors().emplace_back(0xaeadac);
    graphColors().emplace_back(0xd7d6d5);
}

// DarkTheme

DarkTheme::
DarkTheme()
{
    QLinearGradient g { 0.5, 0.0, 0.5, 1.0 };
    g.setColorAt(0.0, QRgb(0x2e303a));
    g.setColorAt(1.0, QRgb(0x121218));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0x86878c) };
    tickLabelColor() = QRgb(0xffffff);

    graphColors().reserve(5);
    graphColors().emplace_back(0x38ad6b);
    graphColors().emplace_back(0x3c84a7);
    graphColors().emplace_back(0xeb8817);
    graphColors().emplace_back(0x7b7f8c);
    graphColors().emplace_back(0xbf593e);
}

// LightTheme

LightTheme::
LightTheme()
{
    QLinearGradient g;
    g.setColorAt(0.0, QRgb(0xffffff));
    g.setColorAt(1.0, QRgb(0xffffff));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0xd6d6d6) };
    tickLabelColor() = QRgb(0x404044);

    graphColors().reserve(5);
    graphColors().emplace_back(0x209fdf);
    graphColors().emplace_back(0x99ca53);
    graphColors().emplace_back(0xf6a625);
    graphColors().emplace_back(0x6d5fd5);
    graphColors().emplace_back(0xbf593e);
}

// BlueCeruleanTheme

BlueCeruleanTheme::
BlueCeruleanTheme()
{
    QLinearGradient g { 0.5, 0.0, 0.5, 1.0 };
    g.setColorAt(0.0, QRgb(0x056189));
    g.setColorAt(1.0, QRgb(0x101a31));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0xd6d6d6) };
    tickLabelColor() = QRgb(0xffffff);

    graphColors().reserve(5);
    graphColors().emplace_back(0xc7e85b);
    graphColors().emplace_back(0x1cb54f);
    graphColors().emplace_back(0x5cbf9b);
    graphColors().emplace_back(0x009fbf);
    graphColors().emplace_back(0xee7392);
}

// BlueIcy

BlueIcyTheme::
BlueIcyTheme()
{
    QLinearGradient g { 0.5, 0.0, 0.5, 1.0 };
    g.setColorAt(0.0, QRgb(0xffffff));
    g.setColorAt(1.0, QRgb(0xffffff));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0xd6d6d6) };
    tickLabelColor() = QRgb(0x404044);

    graphColors().reserve(5);
    graphColors().emplace_back(0x3daeda);
    graphColors().emplace_back(0x2685bf);
    graphColors().emplace_back(0x0c2673);
    graphColors().emplace_back(0x5f3dba);
    graphColors().emplace_back(0x2fa3b4);
}

// BlueNcs

BlueNcsTheme::
BlueNcsTheme()
{
    QLinearGradient g;
    g.setColorAt(0.0, QRgb(0xffffff));
    g.setColorAt(1.0, QRgb(0xffffff));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0xd6d6d6) };
    tickLabelColor() = QRgb(0x404044);

    graphColors().reserve(5);
    graphColors().emplace_back(0x1db0da);
    graphColors().emplace_back(0x1341a6);
    graphColors().emplace_back(0x88d41e);
    graphColors().emplace_back(0xff8e1a);
    graphColors().emplace_back(0x398ca3);
}

// BrownSand

BrownSandTheme::
BrownSandTheme()
{
    QLinearGradient g;
    g.setColorAt(0.0, QRgb(0xf3ece0));
    g.setColorAt(1.0, QRgb(0xf3ece0));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0xb5b0a7) };
    tickLabelColor() = QRgb(0x404044);

    graphColors().reserve(5);
    graphColors().emplace_back(0xb39b72);
    graphColors().emplace_back(0xb3b376);
    graphColors().emplace_back(0xc35660);
    graphColors().emplace_back(0x536780);
    graphColors().emplace_back(0x494345);
}

// HighContrast

HighContrastTheme::
HighContrastTheme()
{
    QLinearGradient g { 0.5, 0.0, 0.5, 1.0 };
    g.setColorAt(0.0, QRgb(0xffffff));
    g.setColorAt(1.0, QRgb(0xffffff));
    g.setCoordinateMode(QGradient::ObjectBoundingMode);
    backgroundBrush() = g;

    axisPen() = QPen { QRgb(0x8c8c8c) };
    tickLabelColor() = QRgb(0x181818);

    graphColors().reserve(5);
    graphColors().emplace_back(0x202020);
    graphColors().emplace_back(0x596a74);
    graphColors().emplace_back(0xffab03);
    graphColors().emplace_back(0x038e9b);
    graphColors().emplace_back(0xff4a41);
}

std::unique_ptr<ColorTheme>
makeColorTheme(QString const& name)
{
    if (name.isEmpty() || name == DefaultTheme::name()) {
        return std::make_unique<DefaultTheme>();
    }
    else if (name == LightTheme::name()) {
        return std::make_unique<LightTheme>();
    }
    else if (name == DarkTheme::name()) {
        return std::make_unique<DarkTheme>();
    }
    else if (name == BlueCeruleanTheme::name()) {
        return std::make_unique<BlueCeruleanTheme>();
    }
    else if (name == BlueIcyTheme::name()) {
        return std::make_unique<BlueIcyTheme>();
    }
    else if (name == BlueNcsTheme::name()) {
        return std::make_unique<BlueNcsTheme>();
    }
    else if (name == BrownSandTheme::name()) {
        return std::make_unique<BrownSandTheme>();
    }
    else if (name == HighContrastTheme::name()) {
        return std::make_unique<HighContrastTheme>();
    }
    else {
        return std::make_unique<DefaultTheme>();
    }
}

std::array<QString, 8>
themeNames()
{
    std::array<QString, 8> names {
        DefaultTheme::name(),
        DarkTheme::name(),
        LightTheme::name(),
        BlueCeruleanTheme::name(),
        BlueIcyTheme::name(),
        BlueNcsTheme::name(),
        BrownSandTheme::name(),
        HighContrastTheme::name(),
    };

    return names;
}

} // namespace qsensors::plot
