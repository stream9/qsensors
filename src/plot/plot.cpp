#include "plot.hpp"

#include "color_theme.hpp"

#include "core/history.hpp"
#include "core/pointer.hpp"
#include "core/sensor.hpp"

#include <boost/numeric/conversion/cast.hpp>

#include <QBrush>
#include <QLinearGradient>
#include <QMouseEvent>
#include <QPen>
#include <QRgb>
#include <QWheelEvent>

namespace qsensors::plot {

Plot::
Plot(QWidget& parent)
    : QCustomPlot { &parent }
    , m_theme { makeColorTheme(QSL("Default")) }
{
    setUpColors();
    assert(m_theme);

    this->legend->setVisible(false);

    QSharedPointer<QCPAxisTickerDateTime> const ticker { new QCPAxisTickerDateTime };
    ticker->setDateTimeFormat("hh:mm:ss");
    this->xAxis->setTicker(ticker);

    this->connect(this, &QCustomPlot::mouseMove,
                  this, &Plot::onMouseMove);
}

Plot::~Plot() = default;

QColor Plot::
graphColor(int const index) const
{
    auto& graph = to_ref(this->graph(index));
    return graph.pen().color();
}

Sensor const* Plot::
sensor(int const index) const
{
    auto& graph = to_ref(this->graph(index));

    //TODO get rid of linear search
    for (auto& [sensor, g]: m_graphs) {
        if (g == &graph) {
            return sensor;
        }
    }

    return nullptr;
}

int Plot::
width() const noexcept
{
    return m_width;
}

void Plot::
setGraphVisible(int const index, bool const visible)
{
    auto& graph = to_ref(this->graph(index));
    graph.setVisible(visible);
    rescale();
    this->replot();
}

void Plot::
setGraphColor(int const index, QColor const& color)
{
    QPen pen;
    pen.setColor(color);
    pen.setWidth(1);

    to_ref(this->graph(index)).setPen(pen);

    Q_EMIT graphColorChanged(index, color);
}

void Plot::
setColorTheme(QString const& name)
{
    m_theme = makeColorTheme(name);

    setUpColors();

    this->replot();
}

void Plot::
setWidth(int w) noexcept
{
    m_width = w < minWidth ? minWidth : w;

    auto now = static_cast<uint>(QDateTime::currentDateTime().currentSecsSinceEpoch());;
    this->xAxis->setRange(now, m_width, Qt::AlignRight);
}

void Plot::
addSensor(Sensor const& s)
{
    auto& graph = to_ref(this->addGraph());
    auto const index = this->graphCount() - 1;

    auto const& color = m_theme->graphColor(index);
    setGraphColor(index, color);

    graph.setName(s.name());

    auto const min = s.minimum().value_or(0);
    auto const max = s.maximum().value_or(0);
    QCPRange range = this->yAxis->range();

    if (range.lower < min) {
        range.lower = min;
    }
    if (range.upper < max) {
        range.upper = max;
    }

    this->yAxis->setRange(range);

    m_graphs[&s] = &graph;
}

void Plot::
addSensor(Sensor const& sensor, History const& history)
{
    addSensor(sensor);

    auto const& records = history.records(sensor);
    auto const nRecords = boost::numeric_cast<int>(records.size());

    QVector<double> keys, values;
    keys.reserve(nRecords);
    values.reserve(nRecords);

    for (auto const& record: records) {
        keys.push_back(boost::numeric_cast<double>(record.time));
        values.push_back(record.value);
    }

    assert(keys.size() == nRecords);
    assert(values.size() == nRecords);

    auto it = m_graphs.find(&sensor);
    assert(it != m_graphs.end());

    auto& graph = to_ref(it->second);

    graph.setData(keys, values, true);
}

void Plot::
clear()
{
    this->clearGraphs();
    m_graphs.clear();
}

void Plot::
update()
{
    auto now =
        static_cast<uint>(QDateTime::currentDateTime().currentSecsSinceEpoch());

    for (auto [sensor, graph]: m_graphs) {
        auto const v = sensor->value().value_or(0);
        graph->addData(now, v);

        auto const& data = graph->data();
        if (data->size() > m_duration) {
            data->remove(data->at(0)->sortKey());
        }
    }

    rescale();
    this->replot();
}

void Plot::
selectGraph(int const index)
{
    auto& graph = to_ref(dynamic_cast<QCPGraph*>(this->plottable(index)));
    selectGraph(graph);
}

void Plot::
unselectGraph()
{
    for (auto* const g: this->selectedPlottables()) {
        g->setSelection(QCPDataSelection());
    }

    this->replot(QCustomPlot::rpQueuedReplot);
}

void Plot::
wheelEvent(QWheelEvent* const ev)
{
    auto const& event = to_ref(ev);
    auto const& delta = event.angleDelta();

    auto const factor = delta.y() > 0 ? 1.1
                      : delta.y() < 0 ? 0.9
                      : 0;
    if (factor == 0) return;

    setWidth(static_cast<int>(m_width * factor));

    this->replot(QCustomPlot::rpQueuedReplot);
}

void Plot::
setUpColors()
{
    this->setBackground(m_theme->backgroundBrush());

    auto const& axisPen = m_theme->axisPen();
    auto setAxisPen = [&](auto& axis, auto& pen) {
        axis.setBasePen(pen);
        axis.setTickPen(pen);
        axis.setSubTickPen(pen);
        axis.setTickLabelColor(pen.color());
        axis.setTickLabelColor(m_theme->tickLabelColor());
    };
    setAxisPen(*this->xAxis, axisPen);
    setAxisPen(*this->yAxis, axisPen);

    for (int i = 0; i < this->graphCount(); ++i) {
        setGraphColor(i, m_theme->graphColor(i));
    }
}

void Plot::
rescale()
{
    auto now =
        static_cast<uint>(QDateTime::currentDateTime().currentSecsSinceEpoch());

    auto xRange = this->xAxis->range();
    this->xAxis->setRange(now, m_width, Qt::AlignRight);

    QCPRange yRange;
    for (auto [_, graph]: m_graphs) {
        bool ok = false;
        auto r = graph->getValueRange(ok, QCP::sdBoth, xRange);
        if (ok) {
            if (yRange.size() == 0) {
                yRange = r;
            }
            else {
                yRange.expand(r);
            }
        }
    }

    auto margin = yRange.size() * 0.1;
    yRange.lower -= margin;
    yRange.upper += margin;

    this->yAxis->setRange(yRange);
}

void Plot::
selectGraph(QCPGraph& graph)
{
    if (graph.selected()) return;

    unselectGraph();

    auto const& data = graph.data();
    QCPDataRange range { 0, data->size() };
    graph.setSelection(QCPDataSelection(range));

    this->replot();
}

void Plot::
onMouseMove(QMouseEvent* const ev)
{
    assert(ev);
    auto const pos = ev->pos();

    auto* const graph = this->plottableAt(pos);
    if (graph) {
        for (auto i = 0; i < this->plottableCount(); ++i) {
            if (graph == this->plottable(i)) {
                Q_EMIT graphHovered(i);
                m_hovered = true;
                return;
            }
        }
    }
    else {
        if (m_hovered) {
            Q_EMIT graphLeaved();
        }

        m_hovered = false;
    }
}

} // namespace qsensors::plot
